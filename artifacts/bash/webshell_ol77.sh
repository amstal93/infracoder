#!/bin/bash

## Service: WebShell for OL77
## Operator: Oracle
## Location: <URL>
## Software: https://cockpit-project.org/
## Image: Oracle Linux 7.7
## Version: <0.2> 
## Tags: [MGT]

# --- check root ---
#if [ "$(id -u)" -ne 0 ]; then
#   echo "This script must be run as root" 
#   exit 1
#fi

readonly SCRIPTNAME=$(basename $0);
readonly YUM_OPTS="-d1 -y";

# --- sources ---
source /usr/local/bin/admin.lib.sh
source /usr/local/bin/install.lib.sh
source /usr/local/bin/oci.lib.sh

# --- settings and variables ---
declare -a PORTS
PORTS+=(22 80)
declare -a NETSERVICES
NETSERVICES+=(ssh http https)
LOGFILE="/var/log/infracoder.log";
OCLOUD_CONFIG="";

# --- install modules ---
main () {
    activate_project;
    create_admin;
    enable_epel;
    install_cockpit;
    install_terraform;
    install_packer;
    install_certbot;
    configure_firewall;
}

# --- execution ---
[[ ! -f "/tmp/project.json" ]] && error "project file missing";
echo "executing script ${SCRIPTNAME}";
sudo bash -c "$(declare -f init); init"
main "$@"
note "https://$(oci-public-ip -g)/cockpit+app/@localhost/system/terminal.html"

exit 0
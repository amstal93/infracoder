#!/bin/bash

## Service: NGINX
## Operator: Oracle
## Location: <URL>
## Software: https://www.nginx.org/
## Image: Oracle Linux 7.7
## Version: <0.2> 
## Tags: [...]

# --- check root ---
if [ "$(id -u)" -ne 0 ]; then
   echo "This script must be run as root" 
   exit 1
fi

# --- sources ---
source /usr/local/bin/admin.lib.sh
source /usr/local/bin/install.lib.sh
source /usr/local/bin/oci.lib.sh

# --- settings and variables ---
declare -a PORTS
PORTS+=(22 80)
declare -a NETSERVICES
NETSERVICES+=(ssh http https)
LOGFILE="/var/log/infracoder.log";
OCLOUD_CONFIG="";

# --- modules ---
main() {
    activate_project;
    create_admin;
    configure_ssh;
    enable_epel;
    install_nginx;
    configure_firewall;
}

# --- execution ---
[[ ! -f "/tmp/project.json" ]] && error "project file missing";
echo_log "installing ${SCRIPTNAME}";
sudo bash -c "$(declare -f init); init"
main "$@"
note "https://$(oci-public-ip -g)/"

exit 0
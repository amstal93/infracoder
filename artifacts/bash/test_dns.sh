#!/bin/bash

## Service: DNS
## Operator: Oracle
## Location: <URL>
## Software: ...
## Image: Oracle Linux 7.7
## Version: <0.2>
## Tags: [...]

# --- sources ---
source /usr/local/bin/admin.lib.sh
source /usr/local/bin/install.lib.sh
source /usr/local/bin/oci.lib.sh

# --- settings and variables ---
readonly SCRIPTNAME=$(basename $0)
readonly YUM_OPTS="-d1 -y"
PUBURL="";
PUBIP="";

# --- modules ---
configure_dns_zone() {
    COMPARTMENT=$(jq -r '.client.configuration[] | .compartment' /etc/ocloud/project.json);
    PROJECT=$(jq -r '.client.configuration[] | .project' /etc/ocloud/project.json);
    PUBIP=$(oci-public-ip -g);
    if [ "$PROJECT" != "" ]; then 
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g")
    else
        PROJECT_="";
    fi

    oci dns zone list \
    --compartment-id $COMPARTMENT \
    --output table \
    --query "data[*].{Name:name, State:\"lifecycle-state\", OCID:id}";

    read -e -p "Please select the zone by OCID: " ZONE;

    FQDN=$(oci dns zone get \
    --zone-name-or-id $ZONE \
    --compartment-id $COMPARTMENT \
    --raw-output \
    --query data.name);

    PUBURL=${PROJECT}.${FQDN};
    
    echo "Creating DNS entry ${PUBURL} for ${PUBIP}";

    oci dns record rrset update \
    --domain "${PUBURL}" \
    --zone-name-or-id $ZONE \
    --rtype 'A' \
    --force \
    --items '[{"domain":"'"${PUBURL}"'","rdata":"'"${PUBIP}"'","rtype":"A","ttl":86400}]'

    oci dns record zone get \
    --zone-name-or-id $ZONE \
    --compartment-id $COMPARTMENT \
    --all \
    --output table \
    --sort-order ASC \
    --query "data.items[*].{Domain:domain, RTYPE:rtype, TTL:ttl}";
}

create_waf() {
    PUBURL=$1;
    PUBIP=$(oci-public-ip -g);
    echo "activating web application firewall";

    oci waas waas-policy create \
    --compartment-id "${COMPARTMENT}" \
    --domain "${FQDN}" \
    --display-name "${PROJECT_}waf" \
    --wait-for-state SUCCEEDED \
    --additional-domains '["'"${PROJECT}"'"]';

    oci dns record rrset update \
    --domain "${PUBURL}" \
    --zone-name-or-id $ZONE \
    --rtype 'CNAME' \
    --force \
    --items '[{"domain":"'"${PUBURL}"'","rdata":"'"${FQDN}"'","rtype":"CNAME","ttl":86400}]';

    oci dns record zone get \
    --zone-name-or-id $ZONE \
    --compartment-id $COMPARTMENT \
    --all \
    --output table \
    --sort-order ASC \
    --query "data.items[*].{Domain:domain, RTYPE:rtype, TTL:ttl}";
}


main () {
    configure_dns_zone;
    create_waf $PUBURL;
}

# --- execution ---
main "$@"

exit 0
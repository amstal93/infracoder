#!/bin/bash

## Service: Install Library
## Operator: Oracle
## Location: <URL>
## Software: https://gitlab.com/tboettjer
## Image: Oracle Linux 7.7
## Version: <0.2> 
## Tags: [MGT]


# --- Runtime Modules for Oracle Linux 7.7
install_certbot() {
    echo "installing certbot";
    wget https://dl.eff.org/certbot-auto;
    mv certbot-auto /usr/local/bin/certbot-auto;
    chown root /usr/local/bin/certbot-auto;
    chmod 0755 /usr/local/bin/certbot-auto;
    success "certbot installed";
}

install_nodejs() {
    echo "installing nodejs";
    yum ${YUM_OPTS} install --disablerepo=ol7_developer_EPEL nodejs node-oracledb-node10;
    curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo;
    rpm --import https://dl.yarnpkg.com/rpm/pubkey.gpg;
    yum install yarn;
    success "nodejs installed, version: $(node -v), npm version: $(npm -v), yarn version: $(yarn --version)";
}

install_nvm() {
    # installing node through the version manager
    NVM_VERSION="v0.33.11";
    NODE_VERSION="10.19.0";
    echo "installing nodejs ${NODE_VERSION} via nvm";
    curl -o- https://raw.githubusercontent.com/creationix/nvm/${NVM_VERSION}/install.sh | bash;
    nvm install $NODE_VERSION;
    curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | tee /etc/yum.repos.d/yarn.repo;
    rpm --import https://dl.yarnpkg.com/rpm/pubkey.gpg;
    yum install yarn;
    success "nodejs installed, version: $(node -v), npm version: $(npm -v), yarn version: $(yarn --version)";
}

# --- docker container ---
install_docker() {
    echo "installing docker";
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    USERNAME=$(jq -r '.client.admin[] | .name' ${OCLOUD_CONFIG});
    # depends on install_python()
    PACKAGE="";
    yum install -y yum-utils device-mapper-persistent-data lvm2;
    yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo;
    # --- grep the latest container-selinux package
    # PACKAGE=$(curl http://mirror.centos.org/centos/7/extras/x86_64/Packages/ | grep container-selinux | grep el7.noarch.rpm | cut -d'=' -f7 | cut -d'>' -f1 | sed -e 's/^"//' -e 's/"$//')
    select_package http://mirror.centos.org/centos/7/extras/x86_64/Packages/;
    yum install -y http://mirror.centos.org/centos/7/extras/x86_64/Packages/${PACKAGE};
    # --- install docker-ce
    yum install ${YUM_OPTS} docker-ce docker-ce-cli containerd.io;
    NETSERVICES+=(docker);
    # Add User to docker group
    usermod -aG docker ${USERNAME};
    # Enable and start Docker
    systemctl enable docker && systemctl start docker;
    sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose;
    sudo chmod +x /usr/local/bin/docker-compose;
    DCPV=$(docker-compose --version);
    success "Docker installed, compose version ${DCPV}";
}

select_package() {
    #"select_package http://mirror.centos.org/centos/7/extras/x86_64/Packages/" return a latest container package
    local URL=$1;
    local HTML=$(curl http://mirror.centos.org/centos/7/extras/x86_64/Packages/ | grep container-selinux | grep el7.noarch.rpm);
    local NAME=${HTML##*"href="};
    PACKAGE=$(echo $NAME  | cut -d'>' -f1 | tr -d '"');
}

# --- Software Modules for Oracle Linux 7.7 ---
install_cli() {
    # install OCI CLI in "PATH=$PATH:</home/$USER/bin/oci>" is appended to /home/$USER/.bashrc
    echo "installing OCI CLI";
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    USERNAME=$(jq -r '.client.admin[] | .name' ${OCLOUD_CONFIG});
    TENANCY=$(jq -r '.client.configuration[] | .tenancy' ${OCLOUD_CONFIG});
    REGION=$(jq -r '.client.configuration[] | .region' ${OCLOUD_CONFIG});
    USERID=$(jq -r '.client.admin[] | .user' ${OCLOUD_CONFIG});
    PHRASE="oracle";
    mkdir /home/${USERNAME}/.oci;
    cd /home/${USERNAME}/.oci;
    wget -P /home/${USERNAME}/.oci https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh;
    chmod +x /home/${USERNAME}/.oci/install.sh;
    chown -R $USERNAME:$USERNAME /home/$USERNAME/;
    su - ${USERNAME} -c "/home/${USERNAME}/.oci/install.sh --accept-all-defaults";
    su - ${USERNAME} -c "/home/${USERNAME}/bin/oci setup oci-cli-rc ";
    su - ${USERNAME} -c "/home/${USERNAME}/bin/oci setup keys --overwrite --passphrase ${PHRASE}";
    PASS=$(openssl rsa -pubout -outform DER -in /home/${USERNAME}/.oci/oci_api_key.pem -passin pass:${PHRASE} | openssl md5 -c | grep stdin | tr -d ' ');
    FINGERPRINT=${PASS##*=};
    wget -P /home/${USERNAME}/.oci https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/config;
    sed -i 's/VAR_TENANCY/'"${TENANCY}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_FINGERPRINT/'"${FINGERPRINT}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_REGION/'"${REGION}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_USER/'"${USERID}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_PHRASE/'"${PHRASE}"'/' /home/${USERNAME}/.oci/config;
    chown -R $USERNAME:$USERNAME /home/${USERNAME}/;
    su - ${USERNAME} -c "/home/${USERNAME}/bin/oci setup repair-file-permissions --file /home/${USERNAME}/.oci/config";
    su - ${USERNAME} -c "export OCI_CLI_SUPPRESS_FILE_PERMISSIONS_WARNING=True";
    success "OCI-CLI installed, Version: $(/home/${USERNAME}/bin/oci -v)";
}

install_cockpit() {
    PORTS+=(443);
    NETSERVICES+=(cockpit);
    echo "installing cockpit";
    yum install ${YUM_OPTS} cockpit;
    sed -i 's;'9090';'443';' /usr/lib/systemd/system/cockpit.socket;
    semanage port -m -t websm_port_t -p tcp 443;
    systemctl enable --now cockpit.socket;
    success "Cockpit installed and running on Port 443";
}

install_git() {
    echo "installing git client";
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    USERNAME=$(jq -r '.client.admin[] | .name' ${OCLOUD_CONFIG});
    EMAIL=$(jq -r '.client.admin[] | .email' ${OCLOUD_CONFIG});
    yum install $YUM_OPTS git;
    su - ${USERNAME} -c "git config --global user.name ${USERNAME}";
    su - ${USERNAME} -c "git config --global user.email ${EMAIL}";
    su - ${USERNAME} -c "git config --global core.editor nano";
    success "Git $(git --version) installed";
}

install_terraform() {
    echo "installing terraform";
    TF_RELEASE="0.12.24";
    cd /tmp;
    yum install ${YUM_OPTS} graphviz;
    wget https://releases.hashicorp.com/terraform/$TF_RELEASE/terraform_${TF_RELEASE}_linux_amd64.zip;
    unzip -o /tmp/terraform_${TF_RELEASE}_linux_amd64.zip -d /usr/local/bin/;
    rm -f /tmp/terraform_${TF_RELEASE}_linux_amd64.zip;
    success "Terraform installed, Version: $(/usr/local/bin/terraform version)";
}

install_packer() {
    echo "installing packer";
    PKR_RELEASE="1.5.5";
    cd /tmp;
    wget https://releases.hashicorp.com/packer/$PKR_RELEASE/packer_${PKR_RELEASE}_linux_amd64.zip;
    unzip -o /tmp/packer_${PKR_RELEASE}_linux_amd64.zip -d /usr/local/bin/;
    rm -f /tmp/packer_${PKR_RELEASE}_linux_amd64.zip;
    success "Packer installed, Version: $(/usr/local/bin/packer version)";
}

install_nginx() {
    PORTS+=(8080);
    NETSERVICES+=(nginx);
    echo "installing NGINX";
    yum install ${YUM_OPTS} nginx;
    sed -i 's;80 default_server;8080 default_server; ' /etc/nginx/nginx.conf;
    mv /usr/share/nginx//html/index.html /usr/share/nginx/html/index.orig;
    cd /usr/share/nginx//html/ && wget https://gitlab.com/tboettjer/infracoder/raw/master/artifacts/bash/index.html && cd ~;
    systemctl enable nginx && systemctl start nginx;
    success "NGINX installed and listening at port 8080";
}

_install_vault() {
    echo "installing vault";
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    USERNAME=$(jq -r '.client.admin[] | .name' ${OCLOUD_CONFIG});
    VAULT_RELEASE="1.3.2";
    cd /tmp;
    wget https://releases.hashicorp.com/vault/$VAULT_RELEASE/vault_${VAULT_RELEASE}_linux_amd64.zip;
    unzip -o /tmp/vault_${VAULT_RELEASE}_linux_amd64.zip -d /usr/local/bin/;
    rm -f /tmp/vault_${VAULT_RELEASE}_linux_amd64.zip;
    success "Vault installed, Version: $($USERNAME bash -c 'vault version')";
}

_install_fluxbox() {
    echo "installing fluxbox";
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    USERNAME=$(jq -r '.client.admin[] | .name' ${OCLOUD_CONFIG});
    yum install ${YUM_OPTS} fluxbox xterm xmessage xorg-x11-fonts-misc tigervnc-server;
    
    su - ${USERNAME} -c "\
        mkdir .vnc; \
        echo \"${PASSWORD}\" |  vncpasswd -f > .vnc/passwd; \
        chmod 0600 .vnc/passwd; \
        vncserver; \
        sleep 5;
        vncserver -kill :1; \
        sed -i -e 's!/etc/X11/xinit/xinitrc!/usr/bin/fluxbox!' .vnc/xstartup; \
        ";
    success "Fluxbox installed";
}

_install_postfix() {
    echo "installing postfix";
    yum install postfix;
    systemctl enable postfix && systemctl start postfix;
    success "postfix installed, $(postconf mail_version)";
}

_install_gitlab() {
    # Depends on postfix
    echo "installing gitlab server";
    EXTERNAL_URL="https://gitlab.ocilabs.io";
    cd /tmp;
    curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash;
    yum install ${YUM_OPTS} gitlab-ce;
    success "gitlab server installed";
}

# Service configuration modules
gitlab_config() {
    echo "creating project repository on gitlab";
    local SOURCE="https://gitlab.com/tboettjer/infracoder.git";
    GITURL=$1;
    sudo sed -i 's;VAR_GIT;'"${GITURL}"';' /etc/ocloud/project.json; 
    success $GITURL;
    GITNAME=${GITURL##*:};
    SPACE=${GITNAME%/*};
    REPO=$(echo ${GITNAME##*/} | cut -d "." -f 1);
    git clone $GITURL;
    cd $REPO;
    git remote add upstream $SOURCE;
    git fetch upstream;
    git merge upstream/master -m "Infracoder Merge";
    git push -u origin master;
    [[ -z "${REPO}" ]] && error "No directory was created";
}

cockpit_cert() {
  PUBURL=$1;
  CERT_DIR="/etc/cockpit/ws-certs.d/"
  OCLOUD_CONFIG="/etc/ocloud/project.json";
  [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
  PROJECT=$(jq -r '.client.configuration[] | .project' ${OCLOUD_CONFIG});
  sudo cat /etc/letsencrypt/live/${PUBURL}/fullchain.pem >> /tmp/${PROJECT}.cert;
  sudo cat /etc/letsencrypt/live/${PUBURL}/privkey.pem >> /tmp/${PROJECT}.cert;
  sudo cp /tmp/${PROJECT}.cert $CERT_DIR;
  chown root:root "$CERT_DIR"/*.cert
}

_validate_gitname() {
  # --- git_url returns $URLSSH and $URLHTTPS
  GITURL=$(jq -r '.client.configuration[] | .git' ${HOME}}/project.json);
  local NAME=${GITURL##*:};
  SPACE=${NAME%/*};
  REPO=$(echo ${NAME##*/} | cut -d "." -f 1);
  if [[ $GITURL == git* ]]; then
    GITSSH=$GITURL;
    GITHTTPS=$(echo "https://gitlab.com/${SPACE}/${REPO}.git");
  elif [[ $GITURL == https* ]]; then
    GITHTTPS=$GITURL;
    GITSSH=$(echo "git@gitlab:${SPACE}/${REPO}.git");
  else
    echo "This appears not to be a GitLab repository";
    exit 1;
  fi
}
### Python Examples ##################################
##                                                  ##
##  create ADB, download wallet, connect to ADB     ##
##                                                  ##
##  Alexander Boettcher                             ##
######################################################

import oci
import sys
import argparse

# Load the default configuration
config = oci.config.from_file()

parser = argparse.ArgumentParser(description='Query the API for an autonomous database.')
parser.add_argument('-o','--ocid',help='OCID of autonomous database', required=True)
parser.add_argument('-co','--compocid',help='Compartment ocid', required=False)

args = parser.parse_args()


if args.compocid is not None:
  config["compartment"] = args.compocid
  compartment_id = args.compocid  
  print("Using compartment ocid: ",compartment_id)
else:
  compartment_id = config["compartment"]
  print("Using compartment ocid: ",compartment_id)


adb_ocid = args.ocid

def get_workload_type(db_client):
    adb = db_client.get_autonomous_database(adb_ocid)
    return adb.data.db_workload

def get_lifecycle_state(db_client):
    adb = db_client.get_autonomous_database(adb_ocid)
    return adb.data.lifecycle_state

def get_display_name(db_client):
    adb = db_client.get_autonomous_database(adb_ocid)
    adb.id = adb_ocid

    if adb != None: 
     return adb.data.display_name
    else:
     return "null"


if __name__ == "__main__":
    # Initialize the client
  
    db_client = oci.database.DatabaseClient(config)
    try:
      print(get_display_name(db_client)+" ("+get_workload_type(db_client)+") is in state "+get_lifecycle_state(db_client))
    except:
      print("ADB not existent in compartment ",compartment_id)





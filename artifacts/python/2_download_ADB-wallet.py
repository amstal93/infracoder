### Python Examples ##################################
##                                                  ##
##  create ADB, download wallet, connect to ADB     ##
##                                                  ##
##  Alexander Boettcher                             ##
######################################################

import oci
import sys
import argparse
import os


# Load the default configuration
config = oci.config.from_file()

parser = argparse.ArgumentParser(description='Download an autonomous database wallet.')
parser.add_argument('-o','--ocid',help='OCID of Autonomous database', required=True)
parser.add_argument('-co','--compocid',help='Compartment ocid', required=False)
parser.add_argument('-p','--walletpwd',help='Compartment ocid', required=True)

args = parser.parse_args()
 
if args.compocid is not None:
  config["compartment"] = args.compocid
  compartment_id = args.compocid  
  print("Using compartment ocid: ",compartment_id)
else:
  compartment_id = config["compartment"]
  print("Using compartment ocid: ",compartment_id)

adb_ocid = args.ocid

def get_display_name(db_client):
    adb = db_client.get_autonomous_database(adb_ocid)
    adb.id = adb_ocid

    if adb != None: 
     return adb.data.display_name
    else:
     return "null"


def get_lifecycle_state(db_client):
    adb = db_client.get_autonomous_database(adb_ocid)
    return adb.data.lifecycle_state


def write_sqlnet_ora():
    f=open(os.getcwd()+"/wallet/sqlnet.ora","w+")
    f.write('WALLET_LOCATION = (SOURCE = (METHOD = file) (METHOD_DATA = (DIRECTORY="'+os.getcwd()+'/wallet")))\n')
    f.write("SSL_SERVER_DN_MATCH=yes")

    f.close()
    print("sqlnet.ora modified.")


def download_oracle_instant_client():
    print("########## Getting Oracle Instantclient #####")
    os.system("wget https://download.oracle.com/otn_software/linux/instantclient/193000/instantclient-basic-linux.x64-19.3.0.0.0dbru.zip")
    os.system("unzip instantclient-basic-linux.x64-19.3.0.0.0dbru.zip")
    os.system("rm instantclient-basic-linux.x64-19.3.0.0.0dbru.zip")
    print(" ")
    print("-----Please set this environment for database connect now: ------")
    print("export LD_LIBRARY_PATH="+os.getcwd()+"/instantclient_19_3:$LD_LIBRARY_PATH")
    print("export TNS_ADMIN="+os.getcwd()+"/wallet")


def download_wallet(db_client, ocid):
    if ((get_lifecycle_state(db_client) == 'AVAILABLE') or (get_lifecycle_state(db_client) == 'STOPPED')):
      wallet_name = "wallet_"+get_display_name(db_client)+".zip"
      os.system("oci db autonomous-database generate-wallet --autonomous-database-id "+ocid+" --password "+args.walletpwd+" --file "+wallet_name)
      print("ADB wallet downloaded to file "+wallet_name)
      os.system("unzip "+wallet_name+" -d wallet")
      print("wallet extracted to ./wallet/")
      print("ADB OCID used: ",ocid)
    else:
      print("ADB not ready to download wallet (yet).")
      sys.exit()

                     
  

if __name__ == "__main__":
    # Initialize the client
 
    db_client = oci.database.DatabaseClient(config)

    download_wallet(db_client,args.ocid)
    download_oracle_instant_client()
    write_sqlnet_ora()

    print("you can then connect to this ADB:")
    print("  ->  python3 ./connect_ADB.py -n "+get_display_name(db_client))


### Python Examples ##################################
##                                                  ##
##  create ADB, download wallet, connect to ADB     ##
##                                                  ##
##  Alexander Boettcher                             ##
######################################################


import cx_Oracle
import os
import time
import json
import argparse


parser = argparse.ArgumentParser(description='Connect to autonomous database.')
parser.add_argument('-n','--name',help='name of Autonomous database', required=True)
args = parser.parse_args()

service_name = args.name+"_tp"
print ("connecting to service "+service_name)
connection = cx_Oracle.connect('admin', 'Welcome1!PYSDK', service_name)


#### Create connection to Autonomous Datawarehouse (ADW)
try:
    cursor = connection.cursor()
    print ("Connect to ADW established")
except:
    print ("ADW connection unsuccessful!")
    exit

try:     
    rs= cursor.execute("create table mytab (id number GENERATED ALWAYS AS IDENTITY, col2 varchar2(30))")
except:
    print ("table exists")

	


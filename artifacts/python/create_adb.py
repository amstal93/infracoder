### Python Examples ##################################
##                                                  ##
##  create ADB, download wallet, connect to ADB     ##
##                                                  ##
##  Alexander Boettcher                             ##
######################################################

import oci
import sys
import argparse


# Load the default configuration
config = oci.config.from_file()

parser = argparse.ArgumentParser(description='Create an autonomous database.')
parser.add_argument('-t','--type', help='Workload type (ATP | ADW)',required=True)
parser.add_argument('-n','--name',help='Name of autonomous database', required=True)
parser.add_argument('-co','--compocid',help='Compartment ocid', required=False)
args = parser.parse_args()
 
if args.compocid is not None:
  config["compartment"] = args.compocid
  compartment_id = args.compocid  
  print("Using compartment ocid: ",compartment_id)
else:
  compartment_id = config["compartment"]
  print("Using compartment ocid: ",compartment_id)

# simple check...
if args.type == "ATP":
  adb_workload = "OLTP"
elif args.type == "ADW":
  adb_workload = "DW"
else:
  print("No valid type for ADB given (",args.type,"). Valid are ATP or ADW.")
  sys.exit()
adb_name = args.name


def create_adb(db_client, p_workload, p_name):
    adb_request = oci.database.models.CreateAutonomousDatabaseDetails()

    adb_request.compartment_id = compartment_id
    adb_request.cpu_core_count = 1
    adb_request.data_storage_size_in_tbs = 1
    adb_request.db_name = p_name
    adb_request.display_name = adb_request.db_name
    adb_request.license_model = adb_request.LICENSE_MODEL_BRING_YOUR_OWN_LICENSE
    adb_request.db_workload = p_workload
    # Remeber this is only an example.  Please change the password.
    adb_request.admin_password = "Welcome1!PYSDK"

    adb_response = db_client.create_autonomous_database(
        create_autonomous_database_details=adb_request,
        retry_strategy=oci.retry.DEFAULT_RETRY_STRATEGY)

    adb_id = adb_response.data.id
    print("Created Autonomous Database {}".format(adb_id))
    return adb_id


if __name__ == "__main__":
    # Initialize the client
 
    db_client = oci.database.DatabaseClient(config)
    adb_id = create_adb(db_client, adb_workload, adb_name)
    print(adb_id)



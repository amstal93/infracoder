# Deployment Plans for the Terraform Service Provider

Topology files enable operators to automate the launch of multi-server solutions. A topology template decribes the components and the mutual dependencies of the infrastructure that empowers a solution. While configuration files are developed in collaboration with developers who understands the internals of a particular application, topology definitions are created together with operators who analyze the requirements and understand the constraints of the systems- and communication architecture. Publishing topology definition files in a catalog provides a self service for developers and business users.

[Terraform](https://www.terraform.io) is a tool that automates deployment processes for operators. It merges predefined services with infrastructure into solution templates. Templates translate deep domain knowledge into build plans. Service manager are enabled to add, change or delete resources on demand, they use deployment plans by simply “invoking” them. Managing build plans, provision and deprovision resources on-demand reduces hosting cost significantly. Terraform can either be installed locally or obtained from a service provider. The [resource manager](https://docs.cloud.oracle.com/iaas/Content/ResourceManager/Concepts/resourcemanager.htm) is a service provided by Oracle, specifically for OCI and [Terraform Cloud](https://www.hashicorp.com/products/terraform/) is a service from HashiCorp, enabling multi-cloud deployments. 

### Syntax

Terraform uses the same syntax as Packer. Templates are also written in [HCL](https://github.com/hashicorp/hcl) but represent topology build plans that are translated into API calls to provision a netowork, deploy applications and configure services like the web application firewall. 

```
<BLOCK TYPE> "<BLOCK LABEL>" "<BLOCK LABEL>" {
  # Block body
  <IDENTIFIER> = <EXPRESSION> # Argument
}
```

Even though HCL is a templating language, [expressions](https://www.terraform.io/docs/configuration/expressions.html) allow to use [scripts](https://blog.gruntwork.io/terraform-tips-tricks-loops-if-statements-and-gotchas-f739bbae55f9) as primitives. With version 0.12 Terraform introduced [loops](https://www.hashicorp.com/blog/hashicorp-terraform-0-12-preview-for-and-for-each/), conditionals, [ternary operations](https://github.com/hashicorp/terraform/issues/22131), counts and provides predefined expressions like [create_before_destroy](https://www.terraform.io/docs/configuration/resources.html#create_before_destroy).

## Core Resources

Using the service discovery process, we created a couple of '*.tf' files in the artifact directory. The main.tf file describes the core infrastructure objects, such as virtual cloud networks (VCN), related network services and a subnet. Compute instances, storage components or higher level services like data bases have not been defined yet. Only a minimum set of resources has been defined.

* Virtual Cloud Network (VCN)
* Default DHCP Options
* Internet Gateway
* Default Route Table
* Default Security List
* Security List
* Subnet



```
resource "oci_core_vcn" "myproject_vcn" {
  cidr_block     = "10.0.0.0/24"
  compartment_id = var.compartment_ocid
  display_name   = "VCN"
  dns_label      = "testvcn"
}

resource "oci_core_subnet" "myproject_sbnt" {
  cidr_block        = "10.0.0.0/25"
  display_name      = "myproject_sbnt"
  dns_label         = "myprojectsbnt"
  security_list_ids = [oci_core_security_list.myproject_sl.id]
  compartment_id    = var.compartment_ocid
  vcn_id            = oci_core_vcn.myproject_vcn.id
  route_table_id    = oci_core_route_table.myproject_rt.id
  dhcp_options_id   = oci_core_vcn.myproject_vcn.default_dhcp_options_id
}

resource "oci_core_internet_gateway" "myproject_igw" {
  compartment_id = var.compartment_ocid
  display_name   = "Test_IG"
  vcn_id         = oci_core_vcn.myproject_vcn.id
}

resource "oci_core_route_table" "myproject_route_table" {
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_vcn.myproject_vcn.id
  display_name   = "myproject_rt"

  route_rules {
    destination       = "0.0.0.0/0"
    destination_type  = "CIDR_BLOCK"
    network_entity_id = oci_core_internet_gateway.myproject_igw.id
  }
}

resource "oci_core_security_list" "myproject_sl" {
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_vcn.myproject_vcn.id
  display_name   = "myproject_sl"

  egress_security_rules {
    protocol    = "6"
    destination = "0.0.0.0/0"
  }

  ingress_security_rules {
    protocol = "6"
    source   = "0.0.0.0/0"

    tcp_options {
      max = "22"
      min = "22"
    }
  }

  ingress_security_rules {
    protocol = "6"
    source   = "0.0.0.0/0"

    tcp_options {
      max = "80"
      min = "80"
    }
  }
  ingress_security_rules {
    protocol = "6"
    source   = "0.0.0.0/0"

    tcp_options {
      max = "443"
      min = "443"
    }
  }
}
```

The objective of a topology template is the deployment of an application. With the [provisioner](https://www.terraform.io/docs/provisioners/index.html) argument we inherit [configuration files for individual server](https://www.terraform.io/docs/provisioners/file.html) or we reference [precomplied artifacts](https://docs.cloud.oracle.com/en-us/iaas/Content/API/SDKDocs/terraformbestpractices.htm). Standard procedure should be the later.

```
## --- admin host ---
resource "oci_core_instance" "webshell" {
  availability_domain = var.ad
  compartment_id      = var.compartment_ocid
  display_name        = "AdminHost"
  shape               = "VM.Standard2.1"

  create_vnic_details {
    subnet_id        = oci_core_subnet.myproject_subnet.id
    display_name     = "primaryvnic"
    assign_public_ip = true
    hostname_label   = "webshell"
  }

  source_details {
    source_type = "image"
    source_id   = var.image
  }

  metadata = {
    ssh_authorized_keys = file(var.ssh_public_key_path)
  }
}
```

With the **output block** file we define the data that is queried and shown at the end of a build process. Terraform stores hundreds of attributes for resources, and as engineers we are just interested in a few like the load balancer IP, VPN address, etc.

```
## --- output ---
output "public_IP" {
  value = data.oci_core_vnic.alfresco_host_vnic.public_ip_address
}
```

### Handling Secrets

HCL code can reside in a single file or is split accross multiple files. All *.tf files in a directory are automatically loaded when running a build process. Initially we start with two files, secrets.tf and main.tf. We store secrets in an own file, [secrets.tf](/topology/templates/tpl_secrets.tf) in order to keep credentials separated from the deployment code. 
  
```
// InfraCoder template for more information please go to https://gitlab.com/tboettjer/infracoder

# --- Tenant Information ---
variable "tenancy_ocid" {
  default = "ocid1.tenancy.oc1..xxx"
}

variable "user_ocid" {
  default = "ocid1.user.oc1..xxx"
}

variable "fingerprint" {
  default = "xxx"
}

variable "private_key_path" {
  default = "~/.oci/oci_api_key.pem"
}

variable "compartment_ocid" {
  default = "ocid1.compartment.oc1..xxxr"
}

variable "region" {
  default = "us-phoenix-1"
}

# --- Authorized public IPs ingress (0.0.0.0/0 means all Internet) ---
variable "authorized_ips" {
  # default = "90.119.77.177/32" # a specific public IP on Internet
  # default = "129.156.0.0/16" # a specific Class B network on Internet
  default = "0.0.0.0/0"
}

# --- variables for BM/VM creation ---
variable "ssh_public_key_path" {
    default = "~/.ssh/id_rsa.pub"
}

variable "ssh_private_key_path" {
    default = "~/.ssh/id_rsa"
}

# --- shared state ---
variable "aws_access_key" {
    default = "xxx"
}

variable "aws_secret_key" {
    default = "xxx"
}
```

New elements in the terraform configuration file are ingress traffic limitation, ssh keys and the backend configuration. With **Authorized Public IP Ingress** we define a variable that restricts access from the internet. This variable is used within the definition of security lists [documentation](https://docs.cloud.oracle.com/iaas/Content/Network/Concepts/securitylists.htm). 

## Build Process

As example we create a deployment template for a [document management server based on Alfresco](topology/templates/tpl_main.tf), before reviewing the template for the [Always Free Account](https://github.com/terraform-providers/terraform-provider-oci/blob/master/examples/always_free/main.tf). 

Before deploying Alfresco we create the secrets.tf file. Instead of recollecting the OCIDs we copy the sources.pkr.hcl from the packer setup and reuse the existing keys the structure described above. 

```
cp ~/infracoder/artifacts/packer/adminhost/sources.pkr.hcl ~/infracoder/topology/terraform.tfvars && nano ~/infracoder/topology/secrets.tf
```
The name space, required for the URL can be retrieved via an oci command

```
oci os ns get
```

We create a customer-access and secret-key to connect the object storage bucket that we created with the [management network](cli.md) via an S3 API. Customer keys are created in the [OCI web console](https://docs.cloud.oracle.com/iaas/Content/Identity/Tasks/managingcredentials.htm#To4):

* Open the profile menu in the web console and click "User Settings".
* On the left side of the page, click "Customer Secret Keys".

Note: The customer secret key is only shown **once** in a pop-up window, it can not be retrieved later.

We add in the secrets.tf file and store a [S3 credentials file](topology/templates/tmpl_credentials) in the location "~/.aws". In order to allow operators to backup the *.tfstate files using the [S3 utils](https://s3tools.org/s3cmd).

```
mkdir ~/.aws/ && touch ~/.aws/credentials
```

The file contains the access and the secret key

```
[default]
aws_access_key_id=...
aws_secret_access_key=...
```

After that we create an image using the existing [packer template](artifacts/packer/alfresco/build.pkr.hcl)

```
cd ~/infrascoder/artifacts/packer/alfresco/ && packer build ~/infrascoder/artifacts/packer/alfresco
```

Packer creates a file called manifest.json that contains the OCID for the image. We use the manifest file to declare a [environment variable](https://www.terraform.io/docs/commands/environment-variables.html) for Terraform.

```
export TF_VAR_image=$(cat ~/infracoder/artifacts/packer/alfresco/manifest.json | jq -r '.builds[-1].artifact_id')
```

... and we create the TF_VAR_ad variable for the availability domain using the 'oci' command

```
export TF_VAR_ad=$(oci iam availability-domain list | jq -r '.data[-1].name')
```

Than we change directory to '~/infracoder' and execute the Terraform workflow with the following steps:

* `terraform init` invokes the configuration settings for the deployment process 
* `terraform validate` checks the written code for syntax errors
* `terraform plan` previews changes before applying
* `terraform apply` provisions reproducible infrastructure

In case of a failure we can debug the output of every terraform command

```
TF_LOG=DEBUG OCI_GO_SDK_DEBUG=v terraform apply
```

The build process returns the public IP of the Afresco host. Calling the IP from a browser shows the login page for Alfresco.

## Testing

The validate command tests the consistency of the Terraform code. In addition to that gruntwork published a couple libraries to ease the development of [automated tests](https://github.com/gruntwork-io/terratest). 


[<<](cli.md) | [+](setup.md) | [>>](packer.md)
# Creating Artifacts with Packer

[Packer](https://www.packer.io/) is an open source tool that automates the build process machine images and container. It allows operators to persist artifacts without storing the binary code. Artifacts merge the operating system with application code into objects that either run on a host, a node or a container. In this example, we use Packer to create a custom image for a WebShell host. The image definition is independent from the service configuration in bash. We add a monitoring agent and create images for multiple regions that refer to the same configuration file. In OCI, images are regional resources. With Packer we define multiple deployment targets, so called "sources", and automte the distribution of images accross region. 

Custom images are launched without any further compilation. Loading artifacts instead of running configuration scripts enables operators to offer self service to application owners and reduces the downtime in case of a service interruption. It also allows to store a bespoke reference in the configuration management database (CMDB) without limiting the flexibilty of launching a servers automtically. The artifact.sh script takes a configuration file as input and generates the definition files for a custom image.

* sources.pkr.hcl - defines a target region(s) for the packer build process 
* build.pkr.hcl - describes the image build process
* location.pkr.hcl - contains location specific information, it is referenced in the sources file
* secrets.pkr.hcl - stores the credentials for the OCI tenant, it is also referenced in the sources file 

## Syntax

Packer templates can either be written in JSON or in Hashicorp's Configuration Language (HCL). We use HCL, a language that was developed to define human- and machine-readable tamplates. It combines JSON compatible expressions with scritping functionality. In HCL resources are defined in **blocks**, whach are classified by type. A block can carry multiple labels, and can contain any number of arguments. This allows to build modular templates and combine configuration files that are application oriented with the installation procedure for system monitoring and management tools. Within blocks **arguments** assign **expressions** or nested blocks to an identifier in the form 'key = value'. A value can be any primitive: a string, number, boolean, object, or a list.

```
BLOCK_TYPE "<BLOCK LABEL>" "<BLOCK LABEL>" {
  <IDENTIFIER> = <EXPRESSION> # Argument
}
```

The complete HCL syntax can be reviewed on [Github](https://github.com/hashicorp/hcl2/blob/master/hcl/hclsyntax/spec.md). It is used by Terraform and Packer, Packer only uses an [subset](https://www.packer.io/docs/configuration/from-1.5/syntax.html), the following [expressions](https://www.packer.io/docs/configuration/from-1.5/expressions.html) are supported.

* *Strings* are double-quoted and can contain any UTF-8 characters. Example: "us-phoenix-1"
* *Numbers* are assumed to be base 10
* *Boolean* values are true and false
* *Arrays* can be made by wrapping it in [ ]. Example: [22, 80, 443]. Arrays can contain primitives, other arrays, and objects.

Comments make it easier to read code. HCL supports single- and multi-line comments. Single line comments start with *#* or *//*, multi-line comments are wrapped in /* and */. Nested block comments are not allowed. A multi-line comment (also known as a block comment) terminates at the first */ found.

## Template Structure 

Definitions templates contain a section for the definition of variables and two blocks, the sources- and the build-block. The sources block represents the target stack for the artifact and the build block contains the declarative code, used to create artifacts. We split variables into two files to separate secrets from the build code and store the source and the build block in separate templates to foster reuse of the source definitions.

```
variable "compartment_ocid" {
  type = string
  default = "VAR_COMPARTMENT"
  description = "The OCID of the compartment, used to generate custom images temporarily"
}
...

source "oracle-oci" "frankfurt" {
  region = "eu-frankfurt-1"
  ...
}

build {
  sources = ["source.oracle-oci.phoenix"]

  provisioner "shell" {
    inline = [
      "sleep 3",
      "sudo -u root echo 'WebShell' > .ImageID"
    ]
  }
  ...
}
```

### Handling Secrets

Separating the secrets from the other variables avoids unintensional exposure of secrets. The secrets.pkr.hcl file contains three sections, tenancy, credentials and keys. It is placed in the same directory with the source.pkr.hcl and build.pkr.hcl, because Packer reads all *.pkr.hcl files in one directory and merges the code for the build process. 

```
# --- tenancy ---
variable "tenancy_ocid" { default = "ocid1.tenancy.oc1..xxx" }
variable "subnet" { default = "ocid1.subnet.oc1.eu-frankfurt-1.xxx" }

# --- credentials ---
variable "user_ocid" { default = "ocid1.user.oc1..xxx" }
variable "fingerprint" { default = "00:08:15:..." }
variable "secrets_file" { default = "/home/*user*/.credentials" } # links to files require the absolute path, bash variables like $HOME or ~ cna not be used

# --- keys ---
variable "ssh_key" { default = "/home/*user*/id_rsa" }
variable "ssh_pubkey" { default = "/home/*user*/id_rsa.pub" }
variable "api_key" { default = "/home/*user*/.oci/oci_api_key.pem" }
variable "api_pubkey" { default = "/home/*user*/.oci/oci_api_key_public.pem" }
```

### Defining Variables

In order to build artifacts automatically, we have stored a couple of templates in a separate directory. These packer file templates contain bash variables in the format VAR_NAME, which are replaced with tenancy settings when an artifact is created. 

```
variable "compartment_ocid" {
  type = string
  default = "VAR_COMPARTMENT"
  description = "The OCID of the compartment, used to generate custom images temporarily"
}

variable "region" {
  type = string
  default = "VAR_REGION"
  description = "Deployment Region for custom images"
}

variable "availability_domain" {
  type = string
  default = "VAR_AD"
  description = "Availability domain for custom images"
}

variable "image_ocid" {
  type = string
  default = "VAR_BASEIMAGE"
  description = "The OCID of the base image for the creation of a custom image"
}

variable "image_name" {
  type = string
  default = "VAR_NAME"
  description = "Name of the custom image in the image store"
}

variable "shape" {
  type = string
  default = "VAR_SHAPE"
  description = "Shape used to build a custom image"
}

variable "vars_file" {
  type = string
  default = "VAR_FILE"
  description = "VAR file for shell scripts"
}
```

### Source Block

The sources block contains identifier, credentials and secrets to create a custom image in a particular region. Packer can create images in multiple tenants and accross regions in one build process. We "just" have to add another source block in the sources file. The source block combines the input variables and region specific settings for the build block. E.g. in the source we contains details like user credentials, the API access token and a region specific OCIDs, so that the build can refer to the collection of details using the ["source.oracle-oci.phoenix"] declaration. 

```
source "oracle-oci" "VAR_LOCATION" {
    tenancy_ocid = var.tenancy_ocid
    region = var.region
    availability_domain = var.availability_domain
    compartment_ocid = var.compartment_ocid
    subnet_ocid = var.subnet_ocid
    user_ocid = var.user_ocid
    fingerprint = var.fingerprint
    base_image_ocid = var.image_ocid
    image_name = var.image_name
    shape = var.shape
    key_file = var.ssh_key
    ssh_username = "opc"
}
```

### Build Block

The build block the deployment code for a custom image. [Build](https://www.packer.io/docs/builders/index.html) blocks are invoked as part of a build in order to create an images. A packer instance can comprise multiple builders, beside OCI there are builders for VirtualBox, VMware, and OCI-C. Calling a component generates a machine image for the respective platform.

* [Provisioners](https://www.packer.io/docs/provisioners/index.html) install and configure software on a running machine prior to that machine being turned into a static image. In our example we use a simple shell script, however any configuration management system like Chef or Puppet can be used as provisioners.
* [Post-processors](https://www.packer.io/docs/post-processors/index.html) take the result of a builder or another process to create a new artifact, e.g. an installation manifest.

In our example, we build an administration host that acts as remote controller for the OCI tenant and secure access to the admin host through a web interface. 

```
build {
  sources = ["source.oracle-oci.VAR_LOCATION"]

  provisioner "shell" {
    inline = [
      "sleep 3",
      "sudo -u root echo 'WebShell' > .ImageID"
    ]
  }

  provisioner "file" {
    source = var.ssh_pubkey
    destination = "/tmp/id_rsa.pub"
  }

  provisioner "file" {
    source = var.ssh_key
    destination = "/tmp/id_rsa"
  }

  provisioner "file" {
    source = var.secrets_file
    destination = "/tmp/.credentials"
  }

  provisioner "file" {
    source = var.vars_file
    destination = "/tmp/.credentials"
  }

  provisioner "shell" {
    inline = [
      "cd /tmp",
      "wget https://gitlab.com/tboettjer/infracoder/raw/master/artifacts/bash/ocloud.sh",
      "chmod +x /tmp/ocloud.sh",
      "/tmp/ocloud.sh VAR_EMAIL VAR_SERVICE"
    ]
  }

  post-processor "manifest" {
    output = "VAR_ARTIFACT_VAR_LOCATION.json"
  }
}
```

## Build Process

The build process creates a artifacts by reading the configuration stored in the source block and build block, launching a server, creating an image form the server and retiring the instance after completion. Before starting the build process, client and server perform a time check. Sometimes the system clocks are out of sync and the build process will not start. A simple time refresh on the client overcomes this obstacle. 

```
sudo ntpdate -u nl.pool.ntp.org
```

The build process is initiated by calling 'packer build' with the location of the template file. Relative directories are read from directory where packer is executed. The location can either be a file containing both blocks or a directory with multiple *.pkr.hcl files. Packer will auomatically merge all files with the *.pkr.hcl extension. This process typically takes a few minutes. 

```
cd ~/infracoder/artifacts/packer/webshell && packer build ~/infracoder/artifacts/packer/webshell
```

When the build is complete, you will find the image in the image store of your tenant, the instance that was used by packer is automatically terminated after the build process. The post processor created a manifest that store the OCID in a file \<service\>_\<location\>.json. 


[<<](terraform.md) | [+](setup.md) | [>>](git.md)
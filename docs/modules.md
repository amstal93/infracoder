# Managing Terraform Modules

The **provider block** defines the target infrastructure for Terraform. Every infrastructure provider offers a unique set of resources and services. The provider block defines which providers can be adressed within a topology template. For cross-regional deployments in OCI we have to define multiple provder blocks. More detailed information regarding the OCI service provider can either be found on the [Terraform website](https://www.terraform.io/docs/providers/oci/index.html) or on [GitHub](https://github.com/terraform-providers/terraform-provider-oci)

```
## --- provider settings ---
provider "oci" {
  region           = var.region
  tenancy_ocid     = var.tenancy_ocid
  user_ocid        = var.user_ocid
  fingerprint      = var.fingerprint
  private_key_path = var.private_key_path
}
```

In the main file, we define and declare resources in blocks. The first block, the **terraform block**, contains basic settings. Terraform is a state-aware provisioning tool. Deployments are captured in a file that maps metadata to identifiers. This allows operators to keep track of the provisioning status of resources. State-awareness is a huge efficiency leaver, it enables incremental changes simply by adjusting the topology description to the desired architecture. Terraform automatically resolves the gap between as-is and to-be to create execution plans that match the desired architecture. 

We configure a remote backend, which means we store the state-files in shared location. To capture every deployment change in a team, every terraform instance uses the same backend. We employ the OCI object store and rely on the [S3 API](https://docs.cloud.oracle.com/iaas/Content/Object/Tasks/s3compatibleapi.htm) to protected the state files with a customer secret key. The complete settings for the backend are stored in the begin of the main.tf file. Defining a version helps to avoid inconsistencies when multiple team members store changes in a single file. 

```
## --- terraform settings ---
terraform {
  required_version = "= 0.12.18"

  backend "s3" {
    endpoint   = "https://<namespace>.compat.objectstorage.<region>.oraclecloud.com"
    region     = "us-phoenix-1"
    bucket     = "tfstate_bucket"
    key        = "example/terraform.tfstate"
    shared_credentials_file = "~/.aws/credentials"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    force_path_style            = true
  }
}
```

**Variables** allow to separate settings from the exectuion code, cache certain values and turn complex configuration files into readable templates. Variables can be declared in the main.tf file, stored in separate definitions files, or invoked from environment variables. 

```
## --- variables ---
variable "image" {}
variable "ad" {}
```

**Data sources** calculate or fetch data from external sources. This allows engineers to build on information defined outside of Terraform, or defined by another separate Terraform configuration.

```
## --- data sources ---
data "oci_core_vnic_attachments" "alfresco_host_vnics" {
  compartment_id      = var.compartment_ocid
  availability_domain = var.ad
  instance_id         = oci_core_instance.alfresco_host.id
}

data "oci_core_vnic" "alfresco_host_vnic" {
  vnic_id = lookup(data.oci_core_vnic_attachments.alfresco_host_vnics.vnic_attachments[0], "vnic_id")
}
```


## Directory Structure

Splitting modules in directories the reflect the IT organization allows to share and improve code in areas of responsibility. We distinguish application owner, network- and database admin and separate network, data and service deployment plans in different direcotries.

```
.
├── network
├── services
└── data
```

### Root Directory

In the root directory we separate the settings from deployment code. Secrets remain separated in an own file, furhtermore we split the provider and the terraform block from the main.tf file and we put variables, datasources and outputs in own files.

```
.
├── main.tf        # --- calls variables, locals and data-sources to create resources
├── secrets.tf     # --— keys and certificates for a tenant
├── provider.tf    # --— holds the crredentials for one or more provider
├── terraform.tf   # --— terraform version and backend configuration
├── vars.tf        # --— variables used in the main.tf file
├── datasources.tf # --— retrieve or compute "external" information
└── outputs.tf     # --— contains outputs from the resources created in main.tf
```

The **main.tf** will not hold resource blocks any longer, but requests for modules. Resource configurations are stored in the modul itself. Using modules allows engineers to split templates in components and separate the code logically. Modules are put into effect by referring to the path in the module resource.

```
## --- Call Network Module ---
module "network" {
  source            = "./network/vcn"
  compartment_ocid  = var.compartment_ocid
  cidr_block        = "10.0.0.0/16"
  display_name      = "ServiceVCN"
  dns_label         = "svcvcn"
}
```

The provider block is stored in a seperate **provider.tf**. Providers are team-wide resources, adding providers or enabling regions usually requires a legal and compliance or a commercial approval. Changing the provider settings should be the result of a governed process.

```
# --- provider oci ---
provider "oci" {
  region = "us-phoenix-1"
  tenancy_ocid = var.tenancy_ocid
  user_ocid = var.user_ocid
  fingerprint = var.fingerprint
  private_key_path = var.private_key_path
}
```

We also store the terraform block in an own **terraform.tf** configuration file, in order to enable central management and audits for state files even in a distributed team.

```
terraform {
  required_version = "= 0.12.18"

  backend "s3" {
    endpoint   = "https://{namespace}.compat.objectstorage.{region}.oraclecloud.com"
    region     = "us-phoenix-1"
    bucket     = "tfstate"
    key        = "terraform.tfstate"
    shared_credentials_file = "~/.aws/credentials"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_requesting_account_id  = true
    skip_get_ec2_platforms      = true
    skip_metadata_api_check     = true
    force_path_style            = true
  }
}
```

## Network Modules

Network modules create a "Virtual Cloud Network (VCN)" with subnets and routing to an internet gateway. Eeach subnet either uses the default security list. By default, TCP traffic on destination port 22 (SSH) will be open from any source port. This will enable you to easily launch a Linux instance and connect via SSH. The rules in the default security list can be modified by using the [oci_core_default_security_list](https://docs.cloud.oracle.com/en-us/iaas/Content/Network/Concepts/securitylists.htm) resource.

```
network
├── vcn
├── subnet
├── regional_subnet
├── security_list
├── security_group
└── api_gateway
```

### Definition Files

Modules are self-contained configuration packages that combine multiple resources that are used together in a separate file structure. Beside the the resource configuration file, module folders contain their own variables, datatsources and output definitions. The same module can be called multiple times, allowing resource configurations to be packaged and re-used. Modules can also call other modules and include child module’s into a topology. 

```
Module
├── vars.tf
├── datasources.tf
├── main.tf
└── outputs.tf
```

### main.tf

Calling child-modules with variables for the parent modules allows to distinguish between environments like dev, stage, uat or prod and avoid running terraform with long list of key-value pairs.In the root module varaibles can be set using CLI options or by refering to environment variables. 

```
## --- Virtual Cloud Network ---
resource "oci_core_vcn" "project_vcn" {
  cidr_block     = var.vcn_cidr
  dns_label      = var.vcn_dns_label
  compartment_id = var.compartment_ocid
  display_name   = var.vcn_display_name
}

## --- internet gateway ---
resource "oci_core_internet_gateway" "project_ig" {
  compartment_id = var.compartment_ocid
  display_name   = "InternetGateway ${var.vcn_display_name}"
  vcn_id         = oci_core_vcn.project_vcn.id
}

## --- route table ---
resource "oci_core_default_route_table" "project_rt" {
  manage_default_resource_id = oci_core_vcn.project_vcn.default_route_table_id

  route_rules {
    destination       = "0.0.0.0/0"
    network_entity_id = oci_core_internet_gateway.project_ig.id
  }
}

## --- subnet ---
resource "oci_core_subnet" "project_sbnt" {
  count                      = length(data.oci_identity_availability_domains.project_ad.availability_domains)
  availability_domain        = lookup(data.oci_identity_availability_domains.project_ad.availability_domains[count.index], "name")
  cidr_block                 = cidrsubnet(var.vcn_cidr, ceil(log(length(data.oci_identity_availability_domains.project_ad.availability_domains) * 2, 2)), count.index)
  display_name               = "Default Subnet ${lookup(data.oci_identity_availability_domains.project_ad.availability_domains[count.index], "name")}"
  compartment_id             = var.compartment_ocid
  vcn_id                     = oci_core_vcn.project_vcn.id
  security_list_ids          = [oci_core_vcn.project_vcn.default_security_list_id]
}
```

With **[tagging](https://docs.cloud.oracle.com/en-us/iaas/Content/Tagging/Tasks/managingtagsandtagnamespaces.htm?Highlight=tagging)** engineers assign own metadata to OCI resources. Tags allow operators to categorize resources by purpose, typ or class. A common use case is to add cost-center information for billing reports. We distinguish defined tags from freeform tags.

```
resource "oci_core_instance" "t" {
    ...

    freeform_tags =  {
           Environment = "Prod"
           Department = "Ops"
   }
    defined_tags = {
        HumanResources.CostCenter = "42"
        Operations.Project = "Beta"
        HumanResources.Environment = "Production"
    }
}
```

### vars.tf

Variables allow to separate settings from the exectuion code, cache certain values and turn complex configuration files into readable templates. Variables can either be stored in variable definitions files, or the file is specified individually with the -var command-line option. 

```
## --- parent variables ---
variable "compartment_ocid" {}
variable "cidr_block" {}
variable "display_name" {}
variable "dns_label"  {}
```
Child modules retrieve variables either from vars.tf files in their subdirectory or inherit values, which were passed through the module block of the calling parent module.


### datasources.tf

The configuration for server instances can be defined either as inline blocks or as separate scripts. Using separate scripts keeps the code more flexible and configurable. For admhost_ol77.sh we use the file interpolation function to read this file from disk. The 'template_file' data source invokes the file interpolation function and renders a template from a the string that is loaded from the external file. The path.module helps to convert the path that is relative to the module folder and this can be one of the structure.

```
data “template_file” “init” {
 template = “${file(“${path.module}/alfresco_ol77.sh”)}”
}
```

With **[remote_state](https://www.terraform.io/docs/providers/terraform/d/remote_state.html)** automation enigeers retrieve state data from a Terraform backend. Root-level outputs of one configuration can be used as input for another configuration. 

```
Usage:
data “terraform_remote_state” “MGT” {
 backend = “ocibucket”
 config  = {
   bucket  = var.tfstate
   key     = var.terraform.tfstate
   region  = var.region
 }
}
 
# Retrieves the id and subnet_ids directly from remote backend state files.
resource “oci_xx_xxxx” “main” {
 # …
 subnets = split(“,”, data.terraform_remote_state.MGT.subnets)
 id = data.terraform_remote_state.MGT.outputs.id
}
```

### outputs.tf

Working with modules requires an extension to the output path. We create an output.tf file in the module directory that maps a name to a value, e.g. the OCID.

```
output "vcn_id" {
    value = oci_core_vcn.vcn.id
}
```

In the root directory, the output.tf refers to the given name in the module path.

```
output "vcn" {
    value       = module.network.vcn_id
    description = "VCN Identifier"
}
```

As a result we can access the name, terraform apply will return the value when the build process finishes.

```
Outputs:
vcn = ocid1.vcn.xxx
```

## Data Modules

Data Modules expose any sort of storage capacity. We compromise object, file or archive storage with databases and data warehouses.

```
data
├── bucket
├── autonomous_transaction_processing
├── autonomous_data_warehouse
└── database_cloud_service
```

## Service Modules

A service module is a discrete unit of functionality that can be accessed remotely and acted upon and updated independently. Services are defined to present interfaces independent of the infrastructure, the service topology and scaling mechanisms. We distinguish between application services, core infrastructure- and management services. 

```
services
├── core
├── application
└── management
```

## References and Blueprints

Great sources of inspiration for writing modules and OCI deployment templates are the  [Oracle Cloud Infrastructure Provider Examples](https://github.com/terraform-providers/terraform-provider-oci/tree/master/examples) and [Terraform Examples for Oracle Cloud](https://github.com/oracle/terraform-examples) repository on GitHub. 


[<<](git.md) | [+](setup.md) | [>>](README.md)
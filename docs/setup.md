## Getting Started

For an introduction to infrastructure as code we need a Gen2 cloud account and access to the InfraCoder repository on GitLab. The easiest way to obtain a tenant is **signing up for [Oracle's Free Tier](https://www.oracle.com/cloud/free/)**. With Gen2 cloud infrastructure Oracle assigns dedicated resources to users. Sometimes that leads to a situation, where new registrations are prevented by a lack of resources in a specific region. In that case, select a different region for the registration helps to voercome the hurdle. The [home region](https://docs.cloud.oracle.com/en-us/iaas/Content/API/Concepts/cloudshellintro.htm) of a user does not constraint the target region for infrastructure deployments. After registering their account, users can activate a different target region through the consol. 

[<img src="docs/freetier.png" width="500">](https://myservices.us.oraclecloud.com/mycloud/signup)

Infracoding is about writing configuration scripts. We recommend three tools for a developer, a modular editor like [Code](https://code.visualstudio.com/download), a local [Git client](https://git-scm.com), and [VirtualBox](https://www.virtualbox.org/). The modular editor can be extended with plugins that make infracoding easier, e.g. the [Terraform](https://marketplace.visualstudio.com/items?itemName=mauve.terraform) plugin for linting and auto-completion. A git client allows for version control and code replication with the central repository. [VirtualBox](https://www.virtualbox.org/wiki/Downloads) enables engineers to run the same image locally that is used to launch services in OCI. For this introduction we use an [Oracle Linux 7.7](http://bit.ly/2EPHgyb) image.

### Boot-Node

The setup process for a new cloud starts with launching a boot-node. We employ the [cloudshell](https://docs.cloud.oracle.com/en-us/iaas/Content/API/Concepts/cloudshellintro.htm) to run an initial setup.sh script. The cloudshell is activated with a click on the button in the upper right corner of the web console. A command line interface is added to the screen and allows to manage OCI ressources through scripts rather than the graphical user interface (UI). We avoid errors by starting the cloudshell while the console is set to the home region. 

[<img src="docs/cloudshell.png" width="500">](https://myservices.us.oraclecloud.com/mycloud/signin)

After starting the [cloudshell](https://docs.cloud.oracle.com/en-us/iaas/Content/API/Concepts/cloudshellintro.htm) we can adjust the region in the console to reflect the desired target region for a project. The launch of the terminal interface will take a few moments. The setup script is loaded from the [infracoder repository](https://gitlab.com/tboettjer/infracoder) and executed to collect the tenant meta data, to create a network and to launch a server. A unique project name helps to manage multiple projects in one account. Names should be limited to characters and numbers, avoiding special characters or empty spaces inorder to consistently use them for DNS entries.

```
bash <(wget -qO- https://gitlab.com/tboettjer/infracoder/raw/master/.admin/setup.sh)
```

During the setup we decide whether we reate a new compartment and a new vault or reuse existing resources. In general, we only create new resources if necessary, hence *avoid deleting resources*. Deleting resources can strain the service limits for a period of time. Therefore, we check whether our tenant contains a compartment and/or a vault that we can reuse and update rather than creating a new one. 

In our example, remote management functions are initially invoked using the [OCI Command Line Interface (CLI)](https://docs.cloud.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm). The CLI is a Python-based tool that adds the oci command to the command line. We use the oci command within a bash script to create a virtual cloud network (VCN), a subnet, enable internet access and launch the boot-node for our cloud. The script offers a few options to define a project, e.g. the admin user, a [deployment region](https://docs.cloud.oracle.com/en-us/iaas/Content/Identity/Tasks/managingregions.htm), a compartment, a vault, generates a SSH key and creates a project configuration file. The script finishes generating the login string for the boot-node. Note: If any errors occure, the command `rm -r ~/setup* ~/project* ~/cloud-init* ~/launch_instance*` deletes all settings but keeps the SSH key pair.

> Learn more about **[Remote Management using the Command Line Interface (CLI)](cli.md)**

### Service Configuration

The next step after launching the boot-node is configuring an infracoder service. The launch process finishes with an URL. We access the machine via [ssh](https://www.ssh.com/ssh/key), an access key is generated and transfered to the server during the boot process.  Our objective is to configure this server to act as initial working environment for infrastructure coder.

```
ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /home/<user>/.ssh/id_rsa opc@<your IP>
```

We can login directly from the cloudshell or from a local terminal. To use a local terminal, we read out the key with `cat ~/.ssh/id_rsa` and store it on our desktop. Usually keys are stored in the `~/.ssh` directory. On Windows this depends on the terminal program, e.g. [putty](https://support.rackspace.com/how-to/log-into-a-linux-server-with-an-ssh-private-key-on-windows/). Once we are logged in, we run the configuration script with the `ocloud` command. 

```
sudo ocloud --launch webshell_ol77.sh
```

In this example we use the *webshell_ol77.sh* script. It updates the operating system and a number of deployment automation tools. This is just an example on how to configure an own jump host. The list of tools can be extended to reflect the work environment, engieners feel comfortable with.  

*  [Cockpit](https://cockpit-project.org/) for remote terminal access via HTTP
*  [Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html) for topology deployments
*  [Packer](https://www.packer.io/intro/getting-started/install.html) to create custom artifacts
*  [OCI-CLI](https://docs.cloud.oracle.com/iaas/Content/API/SDKDocs/cliinstall.htm) to accelerate template development
*  [Git](https://git-scm.com) for version control and code sharing
*  [jq](https://stedolan.github.io/jq/) as JSON parser 


Operations engineers define there own configuration scripts and store them in the 'bash' directory of the project repository. As convention we define script names as follows:

* the service `name` is written in one word and followed by an underscore '_'
* the `_ol77` abbreviation refers to the operating system and the version
* the file exetension `.sh` marks the file as a script. 

The installation steps are captured in the `/var/log/infracoder.log` file. The script finishes with the URL for an initial login. It's a web URL like `https://<your IP>/cockpit+app/@localhost/system/terminal.html`. If the following page shows up in a web browser, the cockpit app is installed correctly. Initially we use a self-signed certificate to establish [transport layer security (TLS)](https://www.ateam-oracle.com/https-and-trust-in-oracle-public-cloud). Opening the page for the first time, the browser will show a security violation and request a confirmation. Firefox users have to accept the risk explicitely, and Chrome users need to [import the certificate manually](https://peacocksoftware.com/blog/make-chrome-auto-accept-your-self-signed-certificate). 

[<img src="docs/cockpit.png" width="500">](https://192.168.56.11/cockpit+app/@localhost/system/terminal.html)

After logging into the terminal, we connect the webshell with the OCI tenant by storing the public API access key in the console. We retrieve the key via the  command line and store it in the clipboard.

```
cat ~/.oci/oci_api_key_public.pem
```

On the OCI web console, we click the username in the top-right corner, click 'User Settings' and 'Add Public Key' to paste the contents of the PEM key in the dialog box. Click 'Add' to complete the setup. The documentation describes the [upload process](https://docs.cloud.oracle.com/iaas/Content/API/Concepts/apisigningkey.htm#How2) in more detail.

<img src="docs/paste_key.png" width="550">

We check whether the main dependencies are installed looking at the fingerprint. The key is correctly stored when the fingerprint matches the fingerprint in `~/.oci/config`. The activation will take a few moments and we can check whether the tenant is successfully linked to the webshell by executing a basic oci command and request the name space of a tenant. If the command fails, we need to wait few minutes and try again. If it still fails, very likely there is an error in the [profile settings](https://docs.cloud.oracle.com/en-us/iaas/Content/Functions/Tasks/functionsconfigureocicli.htm) and we need to run `oci setup config`.

```
oci os ns get
```

We avoid security warning for the "self-signed certificate" with a official certificate that is provided for a fully qualified domain name (FQDN). The registraar [eu.org](https://nic.eu.org/) reserves domain names free of charge. We check whether the desired name `<your_name>.eu.org` is still [available](https://lookup.icann.org/) and use the [OCI domain name service](https://docs.cloud.oracle.com/en-us/iaas/Content/DNS/Concepts/dnszonemanagement.htm) to manage the domain. Eu.org requires us to create a [SOA record](https://en.wikipedia.org/wiki/SOA_record) before completing the registration process. The ocloud command packages the necessary steps. [Let' Encrypt](https://letsencrypt.org/) is an open certification authority for our new domain. 

```
ocloud --letsencrypt 
```

The ocloud command is used to aggregate multiple commands for repeatable tasks in a given project setup. We continously expand the set of arguments, after the inital setup the ocloud command can be updated using `sudo ocloud --update`.

> Learn to write **[Configuration Scripts in Bash](bash.md)**

### Central Repository

Writing [Infrastructure Code](https://en.wikipedia.org/wiki/Infrastructure_as_code) is teamwork. A central repository improves the integrity of cloud services and reduces the engineering efforts by building on reusable assets. The easiest way to get started is using subscription service like [GitLab](https://www.gitlab.com/), [GitHub](https://github.com/) and [Bitbucket](https://bitbucket.org/). We selected GitLab, because it is open source and allows for a seamless transition to a [private repository](https://about.gitlab.com/install/#centos-7). We start the project-repository replicating the [InfraCoder](https://gitlab.com/tboettjer/infracoder) repository, a public source that represents a collection of best practices, collected in various OCI projects. The repository is maintained by a team of Oracle Solution Engineers.

[<img src="docs/gitlab.png" width="500">](https://gitlab.com/users/sign_up)

Before we start, we check whether the git client is correctly installed.  The global settings should reflect the user that we want to continue to use.

```
git --version && git config --list
```

Administrators manage access to the repository through the GitLab UI. User access is granted using the SSH key that we have created earlier. In the terminal, with show the public key and copy it to the clipboard.

```
cat ~/.ssh/id_rsa.pub
```

The public key is added to our personal [GitLab](https://subscription.packtpub.com/book/application_development/9781783986842/2/ch02lvl1sec20/adding-your-ssh-key-to-gitlab) account through the web console before executing the ocloud command.

* After login, click your avatar in the upper right corner and select *Settings*.
* Navigating to SSH Keys and pasting the public key in the Key field. 
* Click the Add key button.

After that we initialize the git repository through the ocloud command. The script creates a directory, collects and stores the InfraCoder files, connects the git client with GitLab and pushes the initial code to the central repository.  

```
ocloud --gitlab git@gitlab.com:<your_namespapce>/<your_repo>.git
```

Instead of using the usual commands `git clone` or `git init` to initialize a local repository we employ [git fetch](https://git-scm.com/docs/git-fetch) to connect our repository upstream with the InfraCoder source, to enable an easy downstream update when new infrastructure code is published. 

```
git pull upstream master && git push origin master
```

After the initial deployment the ocloud command allows operators to pull the latest update from the infracoder repository and push this changes to the central repository.

> Introducing **[Code Sharing via GitLab](git.md)**

### Deployment Plans

Terraform is the default tool for deployment plans of infrastructure topologies. State-awareness allows for incremental adjustments and rapid responses to changing requirements and helps operators to align infrastructure provisiong with the code changes in a SCRUM project. On OCI, terraform can be executed form the command line or using the the [ressource manager](https://docs.cloud.oracle.com/en-us/iaas/Content/ResourceManager/Concepts/resourcemanager.htm). First we check whether terraform is correctly installed.


```
terraform --version
```

Teams need to ensure that all members work with the same version to avoid corrupt statefiles. On the boot-node we use ocloud command to create a consistent set of terraform blocks. The version is set in the `terraform.tf` file. Team member running terraform locally need to maintin the same version.

```
ocloud --terraform
```

Modules allow to organize infrastructure code and separate administrative tasks. We start a deployment plan, pasting a network module into `main.tf`. The vcn module that can be found at `https://gitlab.com/tboettjer/infracoder/-/tree/master/topology/network/vcn`. 

```
module "vcn" {
  source               = "git::https://gitlab.com/tboettjer/infracoder.git//topology/network/vcn/"
  compartment_ocid     = var.compartment_ocid
  cidr                 = "10.0.0.0/16"
  dns_label            = "vcn"
  display_name         = "main_vcn"
  subnet_dns_label     = "subnet"
  subnet_display_name  = "regional_subnet"
}
```

Terraform templates compromise several layers of resources and services. Common guidelines and naming conventions are required to maintain consistency and allow reuse of infrastructure code in a team. With 'terraform init' we generate an initial state file. 

```
cd ~/<myproject>/topology/ && terraform init
```

We validate the HCL code using the 'validate' option. In our case, it is more about testing the topology_function() code. However, any errors should be corrected in the templates directly.

```
terraform validate
```

We iterate the code until it doesn't throw any errors anymore, before we move towards deployment planning. The plan matches the current architecture agianst the desired architecture and reports any deviations. 

```
terraform plan
```

Running the command for the first time the current architecture doesn't hold any resources, it will propose to create the vcn and subnet from scratch. Existing resources, created through the cli, are not captured, as these are not captured in the state file. After checking the list deployments we execute the provisioning process. Terraform will create a new vcn, a subnet and the necessary network services.  

```
terraform apply
```

Note: After executing the deployment plan we have two vcn in our compartment. Both vcn are configured using the same CIDR range. OCI network virtualization allows independent IP ranges per vcn, traffic forwarding inside the OCI uses encapsulating, hence the CIDR ranges in seperate vcn can overlap. Before proceeding with code sharing, we delete the vcn that was created, using the oci cli.

> Gain efficency writing **[Deployment Plans in Terraform](modules.md)**

### Artifacts

Artifacts allow operators to automate application deployments and pull rather than push server configurations. Instead of building infrastructure before installing applications, the topology template includes a reference to a custome image or a container. We use [Packer](https://www.packer.io/) to precompile and prepackage server configurations and we decouple the build from the deployment process by using custom images or the OCI registry. This allows to introduce self-service and leads to an auditable build process. Artifacts combine operating systems with applications. Operator distinguish three classes of artifacts:

* Host-based services, running on multi purpose machines and limited to operating system tools for deployment automation
* Node-based services, represented by immutable server with application code stored in repositories and automated using configuration scripts
* Container-based services with isolated runtimes that are assmebled pulling artifacts from a registry 

While an artifacts are [regional resources](https://docs.cloud.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm) in OCI, we use a build process to manage the packer templates independent from a specific region. We embedded the packer build process in a bash script that creates a directory for the artifact adjusts the settings and creates a custom image in the OCI images store. First we check whether packer is installed correctly, we need version 1.5.4 or above.

```
packer --version
```

The same templates facilitate the build process for both, bare-metal- or virtual server in order to simplify the process of addressing volatile capacity requirements. For the build process, we use 2.1 shapes, on a freetier the default setting should be changed to `VM.Standard.E2.1.Micro` when the trial period is over. 

```
ocloud --packer webshell_ol77.sh
```

The script builds artifacts from configuration files in the infracoder repository. The ocloud command creates a directory under '~/infracoder/artifacts/packer'. It contains four files with region specific settings, tenant credentials, variables and the build procedure. The build process finishes with a manifest that is stored in the packer directory and contains the [OCID](https://docs.cloud.oracle.com/en-us/iaas/Content/General/Concepts/identifiers.htm) for our custom image. The OCID links the image creation process with the topology definition in Terraform.

> Details on **[Creating Artifacts with Packer](packer.md)**

[<<](README.md) | [+](setup.md) | [>>](bash.md)
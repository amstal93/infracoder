## --- Virtual Cloud Network ---
resource "oci_core_vcn" "main_vcn" {
  cidr_block           = var.cidr
  compartment_id       = var.compartment_ocid
  dns_label            = var.dns_label
  display_name         = var.display_name
}

## --- internet gateway ---
resource "oci_core_internet_gateway" "main_ig" {
  compartment_id = var.compartment_ocid
  display_name   = "InternetGateway ${var.display_name}"
  vcn_id         = oci_core_vcn.main_vcn.id
}

## --- route table ---
resource "oci_core_default_route_table" "main_rt" {
  manage_default_resource_id = oci_core_vcn.main_vcn.default_route_table_id

  route_rules {
    destination       = "0.0.0.0/0"
    network_entity_id = oci_core_internet_gateway.main_ig.id
  }
}

## --- regional subnets ---
module "regional_subnets" {
  source               = "git::https://gitlab.com/tboettjer/infracoder.git//topology/network/subnet_regional/"
  compartment_ocid     = var.compartment_ocid
  vcn_id               = oci_core_vcn.main_vcn.id
  subnets              = {
    frontend  = "10.0.0.0/24"
    backend   = "10.0.1.0/24"
    data      = "10.0.2.0/24"
  }
  region               = var.region
  state_file           = var.state_file
  state_bucket         = var.state_bucket
}

## --- security list ---
module "main_security_list" {
  source                 = "git::https://gitlab.com/tboettjer/infracoder.git//topology/network/security_list"
  compartment_ocid       = var.compartment_ocid
  vcn_id                 = oci_core_vcn.main_vcn.id
  sl_display_name        = "main_security_list"
  egress_destination     = "0.0.0.0/0"
  egress_protocol        = "all"
  ingress_stateless      = "false"
  ingress_rules          = [
    {
      protocol_nr = "6"
      port_min    = 22
      port_max    = 22
      source_cidr = "0.0.0.0/0"
    },
    {
      protocol_nr = "6"
      port_min    = 80
      port_max    = 80
      source_cidr = "0.0.0.0/0"
    },
    {
      protocol_nr = "6"
      port_min    = 443
      port_max    = 443
      source_cidr = "0.0.0.0/0"
    },
  ]
}
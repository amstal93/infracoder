variable "cidr" {
  description = "A VCN covers a single, contiguous IPv4 CIDR block of your choice. "
}

variable "compartment_ocid" {
  description = "Compartment's OCID where VCN will be created. "
}

variable "dns_label" {
  description = "A DNS label for the VCN, used in conjunction with the VNIC's hostname and subnet's DNS label to form a fully qualified domain name (FQDN) for each VNIC within this subnet. "
}

variable "display_name" {
  description = "Name of Virtual Cloud Network. "
}

variable "region" {
  description = "The deployment region of the state bucket"
  type        = string
}

variable "state_file" {
  description = "name of the state file"
  type        = string
}

variable "state_bucket" {
  description = "name of the state bucket"
  type        = string
}
### main.tf

```
module "main_vcn" {
  source               = "git::https://gitlab.com/tboettjer/infracoder.git//topology/network/vcn/"
  compartment_ocid     = var.compartment_ocid
  cidr                 = "10.0.0.0/16"
  dns_label            = "vcn"
  display_name         = "main_vcn"
  region               = var.region
  state_file           = var.state_file
  state_bucket         = var.state_bucket
}
```

### output.tf
```
output "vcn_id" {
  value       = module.main_vcn.vcn_id
  description = "ocid of created VCN. "
}

output "internet_gateway_id" {
  value       = module.main_vcn.internet_gateway_id
  description = "ocid of internet gateway. "
}

output "default_route_table_id" {
  value       = module.main_vcn.default_route_table_id
  description = "ocid of default route table. "
}

output "default_security_list_id" {
  value       = module.main_vcn.default_security_list_id
  description = "ocid of default security list. "
}

output "default_dhcp_options_id" {
  value       = module.main_vcn.default_dhcp_options_id
  description = "ocid of default DHCP options. "
}
```
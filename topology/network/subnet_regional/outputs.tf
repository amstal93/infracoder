output "subnet_id" {
  value = [for subnet in oci_core_subnet.regional_subnets : subnet.id]
}
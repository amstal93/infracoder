data "terraform_remote_state" "main_vcn" {
  backend = "s3"
  config = {
    endpoint   = var.state_file
    region     = var.region
    bucket     = var.state_bucket
    key        = "terraform.tfstate"
    shared_credentials_file = "~/.aws/credentials"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    force_path_style            = true
  }
}
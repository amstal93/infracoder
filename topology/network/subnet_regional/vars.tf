variable "compartment_ocid" {
  description = "The compartment identifier for the subnet"
  type        = string
}

variable "vcn_id" {
  description = "The VCN for the subnet"
  type        = string
}

variable "subnets" {
  description = "CIDR ranges for the subnets"
  type        = map(string)
}

variable "region" {
  description = "The deployment region of the state bucket"
  type        = string
}

variable "state_file" {
  description = "name of the state file"
  type        = string
}

variable "state_bucket" {
  description = "name of the state bucket"
  type        = string
}
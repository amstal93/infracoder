#############################################################
###### network.tf ###########################################
######                                                  #####
###### OCI Exadata Cloud Service Network Implementation #####
###### Based on Oracle documentation as of 2019-09-18   #####
######                                                  #####
###### contact: alexander.boettcher@oracle.com          #####
#############################################################

#########  VCN definition #############

resource "oci_core_virtual_network" "VCN1" {
  cidr_block     = var.VCN_CIDR
  compartment_id = var.compartment_ocid
  display_name   = var.VCN_NAME
  dns_label      = var.VCN_NAME
}

#########  Internet Gateway definition #############

resource "oci_core_internet_gateway" "IGW" {
  compartment_id = var.compartment_ocid
  display_name   = "IGW"
  vcn_id         = oci_core_virtual_network.VCN1.id
}

#########  NAT Gateway definition  ##############

resource "oci_core_nat_gateway" "NATGW" {
  compartment_id = var.compartment_ocid
  display_name   = "NATGateway"
  vcn_id         = oci_core_virtual_network.VCN1.id
}

#########  DRG definition  ##############
resource "oci_core_drg" "DRG1" {
  #Required
  compartment_id = var.compartment_ocid
  display_name   = "DRG"
}

######################### DRG Attachment ####################

#resource "oci_core_drg_attachment" "vcn_drg_attachment" {
    #Required
#    drg_id = "oci_core_drg.DRG1.id"
#    vcn_id = "oci_core_virtual_network.VCN1.id"

    #Optional for transit routing
    #display_name = "${var.drg_attachment_display_name}"
    #route_table_id = "${oci_core_route_table.test_route_table.id}"
#}

#########  Service Gateway definition  ##############

resource "oci_core_service_gateway" "sg1" {
  display_name   = "ServiceGateway"
  compartment_id = var.compartment_ocid
  services {
    #Required
    service_id = data.oci_core_services.service_gateway_all_oci_services.services[0].id
  }
  vcn_id = oci_core_virtual_network.VCN1.id
}

#########  Route Tables definition #############

#resource "oci_core_route_table" "SGW_RT" {
#  compartment_id = "${var.compartment_ocid}"
#  vcn_id         = "${oci_core_virtual_network.VCN1.id}"
#  display_name   = "SGW_RT"
#}

resource "oci_core_route_table" "RT_ExaClientSN" {
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id
  display_name   = "RT_ExaClient"

  route_rules {
    cidr_block        = "0.0.0.0/0"
    network_entity_id = oci_core_nat_gateway.NATGW.id
  }

  route_rules {
    destination_type  = "SERVICE_CIDR_BLOCK"
    destination       = data.oci_core_services.service_gateway_all_oci_services.services[0]["cidr_block"]
    network_entity_id = oci_core_service_gateway.sg1.id
  }

  route_rules {
    cidr_block        = var.ALL_ON_PREM_CIDR
    network_entity_id = oci_core_drg.DRG1.id
  }
}

resource "oci_core_route_table" "RT_ExaBackupSN" {
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id
  display_name   = "RT_ExaBackup"

  route_rules {
    destination_type  = "SERVICE_CIDR_BLOCK"
    destination       = data.oci_core_services.service_gateway_all_oci_services.services[0]["cidr_block"]
    network_entity_id = oci_core_service_gateway.sg1.id
  }
}

resource "oci_core_route_table" "RT_Windows" {
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id
  display_name   = "RT_Windows"

  route_rules {
    cidr_block        = "0.0.0.0/0"
    network_entity_id = oci_core_nat_gateway.NATGW.id
  }

  route_rules {
    cidr_block        = var.ALL_ON_PREM_CIDR
    network_entity_id = oci_core_drg.DRG1.id
  }

  route_rules {
    cidr_block        = var.SAP_ON_PREM_CIDR
    network_entity_id = oci_core_drg.DRG1.id
  }


}


resource "oci_core_route_table" "RT_Linux" {
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id
  display_name   = "RT_Linux"

  route_rules {
    cidr_block        = "0.0.0.0/0"
    network_entity_id = oci_core_nat_gateway.NATGW.id
  }

  route_rules {
    cidr_block        = var.ALL_ON_PREM_CIDR
    network_entity_id = oci_core_drg.DRG1.id
  }

  route_rules {
    cidr_block        = var.SAP_ON_PREM_CIDR
    network_entity_id = oci_core_drg.DRG1.id
  }

}


resource "oci_core_route_table" "RT_mgmt" {
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id
  display_name   = "RT_MGMT"

  route_rules {
    cidr_block        = var.All-Internet-CIDR
    network_entity_id = oci_core_internet_gateway.IGW.id
  }

  route_rules {
    cidr_block        = var.ON_PREM_CIDR1
    network_entity_id = oci_core_drg.DRG1.id
  }

  route_rules {
    cidr_block        = var.ON_PREM_CIDR2
    network_entity_id = oci_core_drg.DRG1.id
  }

  route_rules {
    cidr_block        = var.ON_PREM_CIDR3
    network_entity_id = oci_core_drg.DRG1.id
  }

  route_rules {
    cidr_block        = var.ON_PREM_CIDR4
    network_entity_id = oci_core_drg.DRG1.id
  }
}

####### Security Groups  ######

resource "oci_core_network_security_group" "nsg_exaclient" {
  #Required
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id

  display_name = "NSG_ExaClient"
}

resource "oci_core_network_security_group" "nsg_exabackup" {
  #Required
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id

  display_name = "NSG_ExaBackup"
}

resource "oci_core_network_security_group" "nsg_mgmt" {
  #Required
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id

  display_name = "NSG_MGMT"
}

resource "oci_core_network_security_group" "NSG_Windows" {
  #Required
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id

  display_name = "NSG_Windows"
}

resource "oci_core_network_security_group" "NSG_Linux" {
  #Required
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id

  display_name = "NSG_Linux"
}



#########################  NSG Rules ########################

## General ingress rule 1: Allows SSH traffic from anywhere

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_ssh1" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "General ingress rule 1: Allows SSH from on-prem"
  source                    = var.ALL_ON_PREM_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "22"
      min = "22"
    }
  }
}

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_mgmt_ssh" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "General ingress rule 1: Allows Ingress from mgmt NSG"
  source                    = oci_core_network_security_group.nsg_mgmt.id
  source_type               = "NETWORK_SECURITY_GROUP"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "22"
      min = "22"
    }
  }
}

resource "oci_core_network_security_group_security_rule" "rule_nsg_exabackup_ssh1" {
  network_security_group_id = oci_core_network_security_group.nsg_exabackup.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "General ingress rule 1: Allows SSH from on-prem to Exabackup"
  source                    = var.ALL_ON_PREM_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "22"
      min = "22"
    }
  }
}


resource "oci_core_network_security_group_security_rule" "rule_nsg_exabackup_mgmt_ssh" {
  network_security_group_id = oci_core_network_security_group.nsg_exabackup.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "General ingress rule 1: Allows Ingress from mgmt NSG to ExaBackup"
  source                    = oci_core_network_security_group.nsg_mgmt.id
  source_type               = "NETWORK_SECURITY_GROUP"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "22"
      min = "22"
    }
  }
}

### General ingress rule 2: Allows Path MTU Discovery fragmentation messages

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_mtu" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "INGRESS"
  protocol                  = "1"
  description               = "General ingress rule 2: Allows ICMP Path MTU Discovery"
  source                    = var.All-Internet-CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"

  icmp_options {
    type = "3"
    code = "4"
  }
}

resource "oci_core_network_security_group_security_rule" "rule_nsg_exabackup_mtu" {
  network_security_group_id = oci_core_network_security_group.nsg_exabackup.id
  direction                 = "INGRESS"
  protocol                  = "1"
  description               = "General ingress rule 2: Allows ICMP Path MTU Discovery"
  source                    = var.All-Internet-CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"

  icmp_options {
    type = "3"
    code = "4"
  }
}

### General ingress rule 3: Allows connectivity error messages within the VCN

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_connecterror" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "INGRESS"
  protocol                  = "1"
  description               = "General ingress rule 3: Allows ICMP connectivity error messages within the VCN"
  source                    = var.VCN_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"

  icmp_options {
    type = "3"
    #code = "all"
  }
}

resource "oci_core_network_security_group_security_rule" "rule_nsg_exabackup_connecterror" {
  network_security_group_id = oci_core_network_security_group.nsg_exabackup.id
  direction                 = "INGRESS"
  protocol                  = "1"
  description               = "General ingress rule 3: Allows ICMP connectivity error messages within the VCN"
  source                    = var.VCN_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"

  icmp_options {
    type = "3"
    #code = "All"
  }
}

### General egress rule 1: Allows all egress traffic

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_all-egress" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "EGRESS"
  protocol                  = "all"
  description               = "General egress rule 1: Allows all egress traffic"
  destination               = var.All-Internet-CIDR
  destination_type          = "CIDR_BLOCK"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_nsg_exabackup_all-egress" {
  network_security_group_id = oci_core_network_security_group.nsg_exabackup.id
  direction                 = "EGRESS"
  protocol                  = "all"
  description               = "General egress rule 1: Allows all egress traffic"
  destination               = var.All-Internet-CIDR
  destination_type          = "CIDR_BLOCK"
  stateless                 = "false"
}

### Client ingress rule 1: Allows ONS and FAN traffic from within the client subnet

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_ons-fan" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "Client ingress rule 1: Allows ONS and FAN traffic from within the client subnet"
  source                    = var.SN_ExaClient_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "6200"
      min = "6200"
    }
  }
}

### Client ingress rule 2: Allows SQL*NET traffic from within the client subnet

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_sqlnet1" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "Client ingress rule 2: Allows SQL*NET traffic from within the client subnet"
  source                    = var.SN_ExaClient_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "1521"
      min = "1521"
    }
  }
}

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_sqlnet2" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "Client ingress rule 2: Allows SQL*NET traffic from on-prem"
  source                    = var.ALL_ON_PREM_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "1521"
      min = "1521"
    }
  }
}


### Client ingress rule 3: Allows all TCP traffic inside the client subnet

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_tcp-clientSN" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "Client ingress rule 3: Allows all TCP traffic inside the client subnet"
  source                    = var.SN_ExaClient_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"
}

### Client ingress rule 4: Allows all ICMP traffic inside the client subnet

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_icmp-clientSN" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "INGRESS"
  protocol                  = "1"
  description               = "Client ingress rule 4: Allows all ICMP traffic inside the client subnet"
  source                    = var.SN_ExaClient_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"
}

### Client egress rule 1: Allows all TCP traffic inside the client subnet

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_tcp-out_clientSN" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "EGRESS"
  protocol                  = "6"
  description               = "lient egress rule 1: Allows all TCP traffic inside the client subnet"
  destination               = var.SN_ExaClient_CIDR
  destination_type          = "CIDR_BLOCK"
  stateless                 = "false"
}

### Client egress rule 2: Allows all ICMP traffic inside the client subnet

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_icmp-out_clientSN" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "EGRESS"
  protocol                  = "1"
  description               = "Client egress rule 2: Allows all ICMP traffic inside the client subnet"
  destination               = var.SN_ExaClient_CIDR
  destination_type          = "CIDR_BLOCK"
  stateless                 = "false"
}

### Client egress rule 3: Allows all egress traffic

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_all-out" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "EGRESS"
  protocol                  = "all"
  description               = "Client egress rule 3: Allows all egress traffic"
  destination               = var.All-Internet-CIDR
  destination_type          = "CIDR_BLOCK"
  stateless                 = "false"
}

### Backup egress rule: Allows access to Object Storage

resource "oci_core_network_security_group_security_rule" "rule_nsg_exabackup_allow-objectstorage" {
  network_security_group_id = oci_core_network_security_group.nsg_exabackup.id
  direction                 = "EGRESS"
  protocol                  = "6"
  description               = "Backup egress rule: Allows access to Object Storage"
  destination_type          = "SERVICE_CIDR_BLOCK"
  destination               = data.oci_core_services.service_gateway_all_oci_services.services[0]["cidr_block"]
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "443"
      min = "443"
    }
  }
}

###### Additional customer-specific rules for the exaclient network
##### 

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_all-onprem-1521" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "Allow all 10.x.y.z networks to access ExaClient SN on 1521"
  source                    = var.ALL_ON_PREM_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "1521"
      min = "1521"
    }
  }
}

# APEX on Exa
#resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient_all-onprem-8080" {
#  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
#  direction                 = "INGRESS"
#  protocol                  = "6"
#  description               = "Allow all 10.x.y.z networks to access ExaClient SN on 8080"
#  source                    = var.ALL_ON_PREM_CIDR
#  source_type               = "CIDR_BLOCK"
#  stateless                 = "false"
#
#  tcp_options {
#    destination_port_range {
#      #Required
#      max = "8080"
#      min = "8080"
#    }
#  }
#}

resource "oci_core_network_security_group_security_rule" "rule_nsg_exaclient-mgmt_1521-to-exaclient" {
  network_security_group_id = oci_core_network_security_group.nsg_exaclient.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "Ingress 1521 from mgmt (for tunneling via Bastion)"
  source                    = oci_core_network_security_group.nsg_mgmt.id
  source_type               = "NETWORK_SECURITY_GROUP"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "1521"
      min = "1521"
    }
  }
}

####################################################################################################

##### NSG_mgmt:

resource "oci_core_network_security_group_security_rule" "rule_nsg_mgmt_ingress-all-onprem" {
  network_security_group_id = oci_core_network_security_group.nsg_mgmt.id
  direction                 = "INGRESS"
  protocol                  = "all"
  description               = "Ingress all protocols from on-prem"
  source                    = var.ALL_ON_PREM_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_nsg_mgmt_egress-all-onprem" {
  network_security_group_id = oci_core_network_security_group.nsg_mgmt.id
  direction                 = "EGRESS"
  protocol                  = "all"
  description               = "Egress all to on-prem"
  destination_type          = "CIDR_BLOCK"
  destination               = var.ALL_ON_PREM_CIDR
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_nsg_mgmt_ingress-ssh-internet" {
  network_security_group_id = oci_core_network_security_group.nsg_mgmt.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "Ingress SSH from Internet"
  source                    = var.All-Internet-CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "22"
      min = "22"
    }
  }
}

resource "oci_core_network_security_group_security_rule" "rule_nsg_mgmt_exaclient-sqlnet" {
  network_security_group_id = oci_core_network_security_group.nsg_mgmt.id
  direction                 = "EGRESS"
  protocol                  = "6"
  description               = "Egress sqlnet from mgmt nsg to exaclient nsg"
  destination               = oci_core_network_security_group.nsg_exaclient.id
  destination_type          = "NETWORK_SECURITY_GROUP"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "1521"
      min = "1521"
    }
  }
}

#### NSG_Windows

resource "oci_core_network_security_group_security_rule" "rule_NSG_Windows_ingress-all-within" {
  network_security_group_id = oci_core_network_security_group.NSG_Windows.id
  direction                 = "INGRESS"
  protocol                  = "all"
  description               = "Ingress all within Windows SN"
  source                    = var.SN_Windows_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_NSG_Windows_egress-all-within" {
  network_security_group_id = oci_core_network_security_group.NSG_Windows.id
  direction                 = "EGRESS"
  protocol                  = "all"
  description               = "Egress all within Windows SN"
  destination               = var.SN_Windows_CIDR
  destination_type          = "CIDR_BLOCK"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_NSG_Windows_ingress-all-from-onprem2" {
  network_security_group_id = oci_core_network_security_group.NSG_Windows.id
  direction                 = "INGRESS"
  protocol                  = "all"
  description               = "Ingress all from on-prem"
  source                    = var.ALL_ON_PREM_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_NSG_Windows_all-tcp-from-exaclient" {
  network_security_group_id = oci_core_network_security_group.NSG_Windows.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "Ingress all TCP from Exaclient SN"
  source                    = oci_core_network_security_group.nsg_exaclient.id
  source_type               = "NETWORK_SECURITY_GROUP"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_NSG_Windows_all-tcp-to-exaclient" {
  network_security_group_id = oci_core_network_security_group.NSG_Windows.id
  direction                 = "EGRESS"
  protocol                  = "6"
  description               = "Egress all TCP to Exaclient SN"
  destination               = oci_core_network_security_group.nsg_exaclient.id
  destination_type          = "NETWORK_SECURITY_GROUP"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_NSG_Windows_https-to-objectstore" {
  network_security_group_id = oci_core_network_security_group.NSG_Windows.id
  direction                 = "EGRESS"
  protocol                  = "6"
  description               = "Egress https to Object Store"
  destination               = data.oci_core_services.service_gateway_all_oci_services.services[0]["cidr_block"]
  destination_type          = "SERVICE_CIDR_BLOCK"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "443"
      min = "443"
    }
  }
}


#### NSG_Linux

resource "oci_core_network_security_group_security_rule" "rule_NSG_Linux_ingress-all-within" {
  network_security_group_id = oci_core_network_security_group.NSG_Linux.id
  direction                 = "INGRESS"
  protocol                  = "all"
  description               = "Ingress all within Linux SN"
  source                    = var.SN_Linux_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_NSG_Linux_egress-all-within" {
  network_security_group_id = oci_core_network_security_group.NSG_Linux.id
  direction                 = "EGRESS"
  protocol                  = "all"
  description               = "Egress all within Linux SN"
  destination               = var.SN_Linux_CIDR
  destination_type          = "CIDR_BLOCK"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_NSG_Linux_ingress-all-from-onprem2" {
  network_security_group_id = oci_core_network_security_group.NSG_Linux.id
  direction                 = "INGRESS"
  protocol                  = "all"
  description               = "Ingress all from on-prem"
  source                    = var.ALL_ON_PREM_CIDR
  source_type               = "CIDR_BLOCK"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_NSG_Linux_all-tcp-from-exaclient" {
  network_security_group_id = oci_core_network_security_group.NSG_Linux.id
  direction                 = "INGRESS"
  protocol                  = "6"
  description               = "Ingress all TCP from Exaclient SN"
  source                    = oci_core_network_security_group.nsg_exaclient.id
  source_type               = "NETWORK_SECURITY_GROUP"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_NSG_Linux_all-tcp-to-exaclient" {
  network_security_group_id = oci_core_network_security_group.NSG_Linux.id
  direction                 = "EGRESS"
  protocol                  = "6"
  description               = "Egress all TCP to Exaclient SN"
  destination               = oci_core_network_security_group.nsg_exaclient.id
  destination_type          = "NETWORK_SECURITY_GROUP"
  stateless                 = "false"
}

resource "oci_core_network_security_group_security_rule" "rule_NSG_Linux_https-to-objectstore" {
  network_security_group_id = oci_core_network_security_group.NSG_Linux.id
  direction                 = "EGRESS"
  protocol                  = "6"
  description               = "Egress https to Object Store"
  destination               = data.oci_core_services.service_gateway_all_oci_services.services[0]["cidr_block"]
  destination_type          = "SERVICE_CIDR_BLOCK"
  stateless                 = "false"

  tcp_options {
    destination_port_range {
      #Required
      max = "443"
      min = "443"
    }
  }
}



#########################################################################

########  Subnet Definitions #############

resource "oci_core_subnet" "SN_ExaClient" {
  # regional subnet
  cidr_block     = var.SN_ExaClient_CIDR
  display_name   = var.SN_ExaClient
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id

  #dhcp_options_id    = "${data.oci_core_dhcp_options.subnet_dhcp_options.id}"
  dhcp_options_id = oci_core_virtual_network.VCN1.default_dhcp_options_id
  route_table_id  = oci_core_route_table.RT_ExaClientSN.id
  dns_label       = "exaclient"
  prohibit_public_ip_on_vnic = true
}

resource "oci_core_subnet" "SN_ExaBackup" {
  # regional subnet
  cidr_block     = var.SN_ExaBackup_CIDR
  display_name   = var.SN_ExaBackup
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id

  #dhcp_options_id    = "${data.oci_core_dhcp_options.subnet_dhcp_options.id}"
  dhcp_options_id = oci_core_virtual_network.VCN1.default_dhcp_options_id
  route_table_id  = oci_core_route_table.RT_ExaBackupSN.id
  dns_label       = "exabackup"
  prohibit_public_ip_on_vnic = true
}

resource "oci_core_subnet" "SN_mgmt" {
  # regional subnet
  cidr_block     = var.SN_mgmt_CIDR
  display_name   = var.SN_mgmt
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id

  #dhcp_options_id    = "${data.oci_core_dhcp_options.subnet_dhcp_options.id}"
  dhcp_options_id = oci_core_virtual_network.VCN1.default_dhcp_options_id
  route_table_id  = oci_core_route_table.RT_mgmt.id
  dns_label       = "mgmt"
  prohibit_public_ip_on_vnic = true
}

resource "oci_core_subnet" "SN_Windows" {
  # regional subnet
  cidr_block     = var.SN_Windows_CIDR
  display_name   = var.SN_Windows
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id

  #dhcp_options_id    = "${data.oci_core_dhcp_options.subnet_dhcp_options.id}"
  dhcp_options_id = oci_core_virtual_network.VCN1.default_dhcp_options_id
  route_table_id  = oci_core_route_table.RT_Windows.id
  dns_label       = "application"
  prohibit_public_ip_on_vnic = true
}

resource "oci_core_subnet" "SN_Linux" {
  # regional subnet
  cidr_block     = var.SN_Linux_CIDR
  display_name   = var.SN_Linux
  compartment_id = var.compartment_ocid
  vcn_id         = oci_core_virtual_network.VCN1.id

  #dhcp_options_id    = "${data.oci_core_dhcp_options.subnet_dhcp_options.id}"
  dhcp_options_id = oci_core_virtual_network.VCN1.default_dhcp_options_id
  route_table_id  = oci_core_route_table.RT_Linux.id
  dns_label       = "linux"
  prohibit_public_ip_on_vnic = true
}


#############################################################
###### datasources.tf #######################################
######                                                  #####
###### OCI Exadata Cloud Service Network Implementation #####
###### Based on Oracle documentation as of 2019-09-18   #####
######                                                  #####
###### contact: alexander.boettcher@oracle.com          #####
#############################################################

#### Definitions of data sources  ##########
############################################################
#### Used for querying information from OCI which is to be  
#### reused during Terraform run.
############################################################

# Gets a list of Availability Domains
data "oci_identity_availability_domains" "ADs" {
  compartment_id = var.compartment_ocid
}

# Gets the OCID of the OS image to use
data "oci_core_images" "OLImageOCID" {
  compartment_id           = var.compartment_ocid
  operating_system         = var.InstanceOS
  operating_system_version = var.InstanceOSVersion
}

#### Service Gateway All Services #####

data "oci_core_services" "service_gateway_all_oci_services" {
  filter {
    name   = "name"
    values = ["All [A-Za-z0-9]+ Services In Oracle Services Network"]
    regex  = true
  }
}


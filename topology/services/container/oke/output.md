## --- Oracle Kubernetes Engine ---
output "oke_cluster_ocid" {
  value         = module.oke.oke_cluster_ocid
  description   = "OKE Cluster Identifier"
}

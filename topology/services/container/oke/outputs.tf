// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

###########################
# compute instance output #
###########################

output "oke_cluster_ocid" {
  value = "${oci_containerengine_cluster.oke_cluster.id}"
}

// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

##############################
# compute instance variables #
##############################

variable "tenancy_ocid" {}
variable "compartment_ocid" {}
variable "ssh_public_key" {}
variable "instance_shape" {}
variable "image_ocid" {}
variable "availability_domain" {}
variable "subnet_ad1_ocid" {}
variable "subnet_ad2_ocid" {}
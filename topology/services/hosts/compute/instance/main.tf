# Availability Domain 1

resource "oci_core_instance" "compute_instance1" {
  availability_domain = "${lookup(data.oci_identity_availability_domains.ads.availability_domains[var.availability_domain - 2],"name")}"
  compartment_id = "${var.compartment_ocid}"
  display_name = "ApplicationInstance1"
  image = "${var.image_ocid}"
  shape = "${var.instance_shape}"
  subnet_id = "${var.subnet1_ocid}"

  metadata = {
    ssh_authorized_keys = "${var.ssh_public_key}"
  }
}

# Availability Domain 2
resource "oci_core_instance" "compute_instance2" {
  availability_domain = "${lookup(data.oci_identity_availability_domains.ads.availability_domains[var.availability_domain - 1],"name")}"
  compartment_id = "${var.compartment_ocid}"
  display_name = "ApplicationInstance2"
  image = "${var.image_ocid}"
  shape = "${var.instance_shape}"
  subnet_id = "${var.subnet2_ocid}"

  metadata = {
    ssh_authorized_keys = "${var.ssh_public_key}"
  }
}
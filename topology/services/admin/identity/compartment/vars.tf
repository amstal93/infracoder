// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

#########################
# compartment variables #
#########################

variable "tenancy_ocid" {}
variable "group_name" {}


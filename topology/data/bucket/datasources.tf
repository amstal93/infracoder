data "oci_objectstorage_bucket_summaries" "buckets" {
  compartment_id = var.compartment_ocid
  namespace      = data.oci_objectstorage_namespace.ns.namespace

  filter {
    name   = "name"
    values = [oci_objectstorage_bucket.service_bucket.name]
  }
}

data "oci_objectstorage_namespace_metadata" "namespace_metadata" {
  namespace = data.oci_objectstorage_namespace.ns.namespace
}

data "oci_objectstorage_namespace" "ns" {
  #Optional
  compartment_id = var.compartment_ocid
}
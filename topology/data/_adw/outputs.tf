output "autonomous_data_warehouse_admin_password" {
  value = random_string.autonomous_data_warehouse_admin_password.result
}

output "autonomous_data_warehouse_high_connection_string" {
  value = lookup(oci_database_autonomous_database.autonomous_data_warehouse.connection_strings.0.all_connection_strings, "high", "unavailable")
}

output "autonomous_data_warehouses" {
  value = data.oci_database_autonomous_databases.autonomous_data_warehouses.autonomous_databases
}

# output "autonomous_data_warehouse_wallet_password" {
#   value = random_string.autonomous_data_warehouse_wallet_password.result
# }

// See https://docs.cloud.oracle.com/iaas/images/
// Oracle-provided image "Oracle-Autonomous-Linux-7.7-2019.12-0"
variable "image_id" {
  type = map(string)
  default = {
    eu-frankfurt-1 = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaazhlmuvqljodxke2mr2jwv63qiwbzjrawseiorthib45secauck2a"
  }
}

variable "availability_domain" {
    default = 1
}

variable "instance_shape" {
    default = "VM.Standard2.1"
}

variable "instance_count" {
    default = 1
}

variable "region" {
  default = "eu-frankfurt-1"
}

variable "state_file" {
  default = "https://sabanga.compat.objectstorage.eu-frankfurt-1.oraclecloud.com"
}

variable "state_bucket" {
  default = "smartrdev_tfstate"
}
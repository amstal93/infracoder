#!/bin/bash

## Service: ocloud command
## Operator: Oracle
## Location: <URL>
## Software: ...
## Image: Oracle Linux 7.7
## Version: <0.2> 
## Tags: [MGT, OCI, CLI]

# --- settings and variables ---
LOGFILE="/var/log/infracoder.log";

source /usr/local/bin/admin.lib.sh;
source /usr/local/bin/install.lib.sh;
source /usr/local/bin/oci.lib.sh;

usage() {
    cat <<-EOF
        Usage: $0 OPTIONS
        Create a Service for Oracle Cloud Infrastructure (OCI)
        Options:
        --help,             -h            show this text and exit
        --adduser,          -a            create an additional admin user
        --letsencrypt,      -e            create a certificate for a FQDN using "Let's Encrypt"
        --gitlab REPO,      -g REPO       create a repository and fetch the infracoder content for gitlab
        --launch SERVICE,   -l SERVICE    launch a service configuration on the ocloud instance
        --packer SERVICE,   -p SERVICE    create a custom image with packer
        --terraform,        -t            setup terraform for initial deployment 
        --module ARTIFACT   -m ARTIFACT   adding a module to the terraform setup
        --update,           -u            retrieve the lates infracoder updates

EOF
    exit 1
}

missing_parameter() {
  echo "Missing parameter for $1" >&2;
  usage;
}

while [[ $# -gt 0 ]]; do
case "$1" in
    "--help"|"-h")
        usage
        ;;
    "--adduser"|"-a")
        USERID="";
        EMAIL="";
        USERNAME="";
        echo "creating a new admin user ...";
        select_user;
        sudo bash -c "$(declare -f create_user); create_user ${USERNAME} ${USERID}"
        sudo cat /home/${USERNAME}/.ssh/id_rsa.pub;
        shift; shift
        ;;
    "--letsencrypt"|"-e")
        PUBURL="";
        echo "creating a certificate with Let's Encrypt";
        select_dns_zone;
        create_cert $PUBURL;
        cockpit_cert $PUBURL;
        shift; shift
        ;;
    "--gitlab"|"-g")
        GITURL="$2";
        [[ $# -lt 2 ]] && missing_parameter "missing Gitlab repository";
        echo "creating a local git directory and connected it to ${GITURL}";
        gitlab_config $GITURL;
        shift; shift
        ;;
    "--launch"|"-l")
        SERVICE_CONFIG="$2";
        [[ $# -lt 2 ]] && missing_parameter "missing configuration script";
        echo "launching ${SERVICE_CONFIG}";
        launch_service "${SERVICE_CONFIG}";
        shift; shift
        ;;
    "--packer"|"-p")
        SERVICE_CONFIG="$2";
        [[ $# -lt 2 ]] && missing_parameter "missing configuration script";
        echo "creating the packer setup files and creating a custom image for ${SERVICE_CONFIG}";
        packer_definition "${SERVICE_CONFIG}";
        packer_sources;
        packer_build "${SERVICE_CONFIG}";
        packer_secrets "${SERVICE_CONFIG}";
        packer_exec "${SERVICE_CONFIG}";
        shift; shift
        ;;
    "--terraform"|"-t")
        echo "creating the terraform setup files";
        create_bucket;
        create_secretkey;
        terraform_setup;
        shift; shift
        ;;
    "--module"|"-m")
        echo "not working yet";
        shift; shift
        ;;
    "--update"|"-u")
        echo "updating ocloud ...";
        update_ocloud;
        shift; shift
        ;;
    *)
        echo "Invalid argument";
        usage
        ;;
esac
done

exit 0
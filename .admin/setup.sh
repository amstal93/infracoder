#!/bin/bash

## Service: Initial Setup
## Operator: Oracle
## Location: <URL>
## Software: ...
## Image: Oracle Linux 7.7
## Version: <0.2> 
## Tags: [MGT, OCI, CLI]

# --- settings ---
readonly SCRPT=$(basename "$0")

TENANCY="";
COMPARTMENT="";
PROJECT="";
VAULT="";
ENDPOINT="";
REGION="";
AD="";
VCN="";
VCN_CIDR="192.168.11.0/24";
SUBNET="";
SBNT_CIDR="192.168.11.0/25";
SECURITYLIST="";
EGRESS_SECURITY="";
INGRESS_SECURITY="";
declare -a INGRESS_PORTS;
INGRESS_PORTS+=(22 80 443);
GIT="";
USERNAME="";
USERID="";
EMAIL="";
SSH_KEY="${HOME}/.ssh/id_rsa";
OS="Oracle Linux";
VERSION_OS="7.7";
SHAPE="VM.Standard2.1";

# --- functions ---
create_config() {
    read -e -p "Provide a name for your project (only letters and numbers, less than 12 characters): " PROJECT;
    if [ "$PROJECT" != "" ]; then 
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g")
    else
        PROJECT_="";
    fi
    TENANCY=$(oci iam compartment list \
    --raw-output \
    --query 'data[0]."compartment-id"');

    HOME_REGION=$(oci iam region-subscription list | jq '.data[] | select(."is-home-region" == true) | ."region-name"' --raw-output);

    wget -P ${HOME}/ https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/project.json;
    [[ ! -f "${HOME}/project.json" ]] && error "Did not create a project file";
    sed -i 's/VAR_TENANCY/'"${TENANCY}"'/' ${HOME}/project.json; 
    sed -i 's/VAR_PROJECT/'"${PROJECT}"'/' ${HOME}/project.json;
    sed -i 's/VAR_HOME/'"${HOME_REGION}"'/' ${HOME}/project.json;
    success "configuration file created";
}

select_admin() {
    oci iam user list \
    --all \
    --output table \
    --query 'data[].{Name: "name", OCID: "id"}';
    
    read -e -p "Select an admin user by OCID: " USERID;
    EMAIL=$(oci iam user get --user-id ${USERID} | jq -r .data.email);
    USERSELECTION=$(validate_user ${EMAIL});
    read -e -p "Please a linux user name for this admin (only letters and numbers): " -i $USERSELECTION -n 10 USERNAME;
    ssh-keygen -o -t rsa -b 2048 -C "$EMAIL" -q -N "" -f $SSH_KEY;
    [[ -z "${USERID}" ]] && error "Did not create an admin";
    [[ -z "${SSH_KEY}" ]] && error "SSH key missing";
    sed -i 's/VAR_VERSIONOS/'"${VERSION_OS}"'/' ${HOME}/project.json;
    sed -i 's/VAR_OS/'"${OS}"'/' ${HOME}/project.json;
    sed -i 's/VAR_SHAPE/'"${SHAPE}"'/' ${HOME}/project.json;
    sed -i 's/VAR_NAME/'"${USERNAME}"'/' ${HOME}/project.json;
    sed -i 's;VAR_EMAIL;'"${EMAIL}"';' ${HOME}/project.json;
    sed -i 's/VAR_USER/'"${USERID}"'/' ${HOME}/project.json;
    success "${EMAIL} is the project admin, ${SSH_KEY} created";
}

select_compartment() {
    if [[ -z "${COMPARTMENT}" ]]; then
        while true; do
            read -p "Do you want to use an existing compartment (y/n)? " yn
            case $yn in
                [Yy]* ) update_compartment; break;;
                [Nn]* ) create_compartment; return;;
                * ) echo "Please answer yes or no.";;
            esac
        done
    else
        echo "${COMPARTMENT} already defined";
        return 1;
    fi
}

create_compartment() {
    read -e -p "Compartment Name (only letters and numbers): " -n 10 COMPARTMENT_NAME;
    OCLOUD_CONFIG="${HOME}/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    TENANCY=$(jq -r '.client.configuration[] | .tenancy' ${OCLOUD_CONFIG});
    HOME_REGION=$(jq -r '.client.configuration[] | .home' ${OCLOUD_CONFIG});
    PROJECT=$(jq -r '.client.configuration[] | .project' ${OCLOUD_CONFIG});
    if [ "$PROJECT" != "" ]; then 
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g")
    else
        PROJECT_="";
    fi
    COMPARTMENT=$(oci iam compartment create \
    --compartment-id "${TENANCY}" \
    --region "${HOME_REGION}" \
    --description "Compartment for project ${PROJECT}" \
    --name "${COMPARTMENT_NAME}"  | jq -r .data.id);
    [[ -z "${COMPARTMENT}" ]] && error "Did not create a compartment";
    AD=$(oci iam availability-domain list --compartment-id ${COMPARTMENT} --region ${REGION}| jq -r '.data[0] | .name');
    sed -i 's/VAR_AD/'"${AD}"'/' ${HOME}/project.json;
    sed -i 's/VAR_COMPARTMENT/'"${COMPARTMENT}"'/' ${HOME}/project.json;
    success "${PROJECT_}compartment created";
}

update_compartment() {
    oci iam compartment list --output table --query "data[*].{OCID: id, Name: name}";
    read -e -p "Select a compartment by OCID: " COMPARTMENT;
    OCLOUD_CONFIG="${HOME}/project.json";
    COMP_NAME=$(oci iam compartment get --compartment-id "${COMPARTMENT}" | jq -r .data.name);
    HOME_REGION=$(jq -r '.client.configuration[] | .home' ${OCLOUD_CONFIG});
    success "${COMP_Name} compartment selected";
    if [[ -z "${COMPARTMENT}" ]]; then
        echo "creating compartment";
        create_compartment;
    else
        read -e -p "New name for compartment? (only letters and numbers, 10 chars): " -i $COMP_NAME -n 12 COMP_IDENT;
        oci iam compartment update \
        --compartment-id "${COMPARTMENT}" \
        --region "${HOME_REGION}" \
        --description "Compartment for project ${PROJECT}" \
        --name "${COMP_IDENT}";
        [[ -z "${COMPARTMENT}" ]] && error "Did not update the compartment";
        AD=$(oci iam availability-domain list --compartment-id ${COMPARTMENT} --region ${REGION}| jq -r '.data[0] | .name');
        sed -i 's/VAR_AD/'"${AD}"'/' ${HOME}/project.json;
        sed -i 's/VAR_COMPARTMENT/'"${COMPARTMENT}"'/' ${HOME}/project.json;
        success "${PROJECT_}compartment updated";
    fi
}

select_vault() {
    if [[ -z "${VAULT}" ]]; then
        while true; do
            read -p "Do you want to use an existing vault (y/n)? " yn
            case $yn in
                [Yy]* ) update_vault; break;;
                [Nn]* ) create_vault; return 1;;
                * ) echo "Please answer yes or no.";;
        esac
        done
    else
        echo "${VAULT} already defined";
        return 1;
    fi
}

create_vault() {
    VAULT=$(oci kms management vault create \
    --compartment-id "${COMPARTMENT}" \
    --display-name "${PROJECT_}vault" \
    --vault-type "DEFAULT" \
    --wait-for-state ACTIVE | jq -r .data.id);
    [[ -z "${VAULT}" ]] && error "Did not create a vault";
    # --- retrieve endpint --- 
    ENDPOINT=$(oci kms management vault get \
    --vault-id "${VAULT}" \
    --raw-output \
    --query 'data."management-endpoint"');
    [[ -z "${ENDPOINT}" ]] && error "Did not retrieve a vault endpoint";
    # --- create key --- 
    ENCRYPTION_KEY=$(oci kms management key create \
    --compartment-id "${COMPARTMENT}" \
    --display-name "${PROJECT_}default_key" \
    --key-shape '{"algorithm":"AES","length":"16"}' \
    --wait-for-state ENABLED \
    --endpoint "${ENDPOINT}");
    [[ -z "${ENCRYPTION_KEY}" ]] && error "Did not create a default key";
    sed -i 's;VAR_ENDPOINT;'"${ENDPOINT}"';' ${HOME}/project.json;
    success "${PROJECT_}default_key created for ${ENDPOINT}";
} 

update_vault() {
    oci kms management vault list \
    --all \
    --compartment-id "${COMPARTMENT}" \
    --output table \
    --query 'data[?contains("lifecycle-state", `'"ACTIVE"'`)].{"name": "display-name", OCID: "id", "state": "lifecycle-state"}';
    read -e -p "Select a vault by OCID: " VAULT;
    if [[ -z "${VAULT}" ]]; then
        note "creating a new vault";
        create_vault;
    else
        success "creating encryption key in ${VAULT}";
        # --- retrieve endpoint --- 
        ENDPOINT=$(oci kms management vault get \
        --vault-id "${VAULT}" \
        --raw-output \
        --query 'data."management-endpoint"');
        [[ -z "${ENDPOINT}" ]] && error "Did not retrieve a vault endpoint";
        # --- create key --- 
        ENCRYPTION_KEY=$(oci kms management key create \
        --compartment-id "${COMPARTMENT}" \
        --display-name "${PROJECT_}key" \
        --key-shape '{"algorithm":"AES","length":"16"}' \
        --wait-for-state ENABLED \
        --endpoint "${ENDPOINT}");
        [[ -z "${ENCRYPTION_KEY}" ]] && error "Did not create a default key";
        sed -i 's;VAR_ENDPOINT;'"${ENDPOINT}"';' ${HOME}/project.json;
        success "${PROJECT_}key updated for ${ENDPOINT}";
    fi
} 

select_region() {
    oci iam region-subscription list --output table --query 'data[*].{ Name: "region-name", Key: "region-key", Status: status }';
    read -e -p "Select a region by name: " REGION;
    [[ -z "${REGION}" ]] && error "Did not select a region";
    sed -i 's/VAR_REGION/'"${REGION}"'/' ${HOME}/project.json;
    success "${REGION} selected";
}

create_vcn() {
    # creates a VCN in the given $COMPARTMENT, the DNS label may not contain non-alphanumeric charactes and only 15 characters are alowed at most
    local DNSLABEL=$(echo "${PROJECT_}vcn" | tr [A-Z] [a-z] | sed "s/\_//g");
    echo "creating a VCN"
    VCN=$(oci network vcn create \
    --display-name "${PROJECT_}vcn" \
    --wait-for-state AVAILABLE \
    --cidr-block "${VCN_CIDR}" \
    --compartment-id "${COMPARTMENT}" \
    --region "${REGION}" \
    --dns-label "${DNSLABEL}" | jq --raw-output .data.id);
    [[ -z "${VCN}" ]] && error "Did not create a vcn";
    success "${PROJECT_}vcn created with CIDR ${VCN_CIDR} in region ${REGION}";
}

create_subnet() {
    echo "creating subnet";
    
    create_rules;
    
    SECURITYLIST=$(oci network security-list create \
    --region "${REGION}" \
    --display-name "${PROJECT_}sl" \
    --vcn-id "${VCN}" \
    --compartment-id "${COMPARTMENT}" \
    --egress-security-rules "[ ${EGRESS_SECURITY} ]" \
    --ingress-security-rules "${INGRESS_SECURITY}" | jq --compact-output [.data.id]);
    [[ -z "${SECURITYLIST}" ]] && error "Did not create a security list";

    SUBNET=$(oci network subnet create \
    --region "${REGION}" \
    --cidr-block "${SBNT_CIDR}" \
    --compartment-id "${COMPARTMENT}" \
    --display-name "${PROJECT_}sbnt" \
    --vcn-id "${VCN}" \
    --security-list-ids $SECURITYLIST | jq --raw-output .data.id);
    [[ -z "${SUBNET}" ]] && error "Did not create a subnet";
    success "${PROJECT_}sbnt with CIDR ${SBNT_CIDR} created and associated security lists";
}

create_rules() {
    # Egress rules
    EGRESS_SECURITY='{"destination": "0.0.0.0/0", "destination-type": "CIDR_BLOCK", "protocol": "all", "isStateless": false}'
    
    # Ingress rules. The script opens these ports both on network level (security list) as well as on O/S level (firewall-cmd)
    declare -a INGRESS_RULES
    
    for PORT in "${INGRESS_PORTS[@]}"; do
        define_rule () {
            cat <<EOF
            {"source": "0.0.0.0/0", "source-type": "CIDR_BLOCK", "protocol": 6, "isStateless": false, "tcp-options": {"destination-port-range": {"max": $PORT, "min": $PORT}}}
EOF
        };
        INGRESS_RULES+=$(define_rule)    
    done;

    INGRESS_SECURITY=$(echo ${INGRESS_RULES[*]} | jq . -s)
}

enable_communication() {
    echo "creating an Internet Gateway and adding default route";
    
    INTERNET_GATEWAY=$(oci network internet-gateway create \
    --region "${REGION}" \
    --compartment-id "${COMPARTMENT}" \
    --is-enabled true \
    --vcn-id "${VCN}" \
    --display-name "${PROJECT_}igw" | jq --compact-output '. | [{cidrBlock:"0.0.0.0/0",networkEntityId: .data.id}]');
    [[ -z "${INTERNET_GATEWAY}" ]] && error "Did not create an internet gateway";

    ROUTE_TABLE=$(oci network route-table list \
    --region "${REGION}" \
    --compartment-id "${COMPARTMENT}" \
    --vcn-id "${VCN}" | jq --raw-output '.data[].id');
    [[ -z "${ROUTE_TABLE}" ]] && error "Did not create a route table";

    RTUPDATE=$(oci network route-table update \
    --region "${REGION}" \
    --rt-id "${ROUTE_TABLE}" \
    --route-rules "${INTERNET_GATEWAY}" \
    --force);
    
    [[ -z "${RTUPDATE}" ]] && error "Did not update the route table";
    success "Internet gateway created and route table configured, name: ${PROJECT_}igw";
}

create_launchscript() {
    wget -P ${HOME}/ https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/launch_instance.sh;
    chmod +x ${HOME}/launch_instance.sh;
    [[ ! -f "${HOME}/launch_instance.sh" ]] && error "Did not create a project file";
    sed -i 's;VAR_SUBNET;'"${SUBNET}"';' ${HOME}/launch_instance.sh;
    sed -i 's/VAR_IMAGE/'"${IMAGE}"'/' ${HOME}/launch_instance.sh;
    success "launch script created";
}

launch_server() {
    local SERVERNAME="${PROJECT_}bn";
    INITFILE="cloud-init.yml";
    echo "launching ${SERVERNAME}";
    rm -r ${HOME}/*.yml
    wget -P ${HOME}/ https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/${INITFILE};
    [[ ! -f "$HOME/${INITFILE}" ]] && error "Did not create an init file";

    IMAGE=$(oci compute image list \
    --compartment-id "${COMPARTMENT}" \
    --region "${REGION}" \
    --operating-system "${OS}" \
    --operating-system-version "${VERSION_OS}" \
    --shape "${SHAPE}" \
    --sort-by TIMECREATED \
    --raw-output \
    --query 'data[0].id');
    [[ -z "${IMAGE}" ]] && error "Did not receive an Image OCID";
    
    SERVER=$(oci compute instance launch \
    --compartment-id "${COMPARTMENT}" \
    --display-name "${SERVERNAME}" \
    --availability-domain "${AD}" \
    --region "${REGION}" \
    --subnet-id "${SUBNET}" \
    --image-id "${IMAGE}" \
    --shape "${SHAPE}" \
    --ssh-authorized-keys-file ${SSH_KEY}.pub \
    --user-data-file "${HOME}/${INITFILE}" \
    --assign-public-ip true \
    --wait-for-state RUNNING \
    --query 'data.id' \
    --raw-output);
    [[ -z "${SERVER}" ]] && error "Did not launch a Server";

    success "launched ${SERVERNAME} in ${COMPARTMENT}\n";

    PUBLIC_IP=$(oci compute instance list-vnics \
    --instance-id "${SERVER}" \
    --region "${REGION}" \
    --query 'data[0]."public-ip"' \
    --raw-output);
    [[ -z "${PUBLIC_IP}" ]] && error "Did not retrieve a public IP address";

    echo "Getting public IP ${PUBLIC_IP}, waiting for ssh";
    sleep 60;
    declare -a TRANSFERS;
    TRANSFERS+=("${HOME}/project.json" \
    "${HOME}/.ssh/id_rsa" \
    "${HOME}/.ssh/id_rsa.pub");
    for TRANSFER in "${TRANSFERS[@]}"; do
        scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${HOME}/.ssh/id_rsa $TRANSFER opc@${PUBLIC_IP}:/tmp;
        while test $? -gt 0
        do
            echo "Trying again..."
            sleep 15;
            scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${HOME}/.ssh/id_rsa $TRANSFER opc@${PUBLIC_IP}:/tmp;
        done
    done

    printf "======================================\n";
    note "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${SSH_KEY} opc@${PUBLIC_IP}"
    printf "======================================\n";
}

validate_user() {
    # --- "user_name $EMAIL" returns $USERNAME
    local CONTENT=$1;
    NAME=${CONTENT%@*};
    echo ${NAME::8} | tr -dc '[:alnum:]\n\r' | tr '[:upper:]' '[:lower:]';
}

error() {
  echo "$(tput setaf 1)--- ${SCRPT}: $@$(tput sgr 0)" >&2
  exit 1
}

success() {
  echo "$(tput setaf 2)--- ${SCRPT}: $@$(tput sgr 0)"
}

note() {
  echo "$(tput setaf 3)$@$(tput sgr 0)"
}

main() {
    create_config;
    select_admin;
    select_region;
    select_compartment;
    #select_vault;
    create_vcn;
    create_subnet;
    enable_communication;
    create_launchscript;
    launch_server;
}

# --- tenancy setup ---
main "$@";

exit 0
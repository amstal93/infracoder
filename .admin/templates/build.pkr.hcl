build {
  sources = ["source.oracle-oci.VAR_LOCATION"]

  provisioner "shell" {
    inline = [
      "sleep 3",
      "sudo -u root echo 'VAR_ARTIFACT' > .ImageID"
      ]
  }

  provisioner "file" {
    source = "/etc/ocloud/project.json"
    destination = "/tmp/project.json"
  }

  provisioner "file" {
    source = "/home/VAR_NAME/.ssh/id_rsa.pub"
    destination = "/tmp/id_rsa"
  }

  provisioner "file" {
    source = "/home/VAR_NAME/.ssh/id_rsa"
    destination = "/tmp/id_rsa"
  }

  provisioner "shell" {
    script = "../bash/VAR_CONF"
    execute_command = "sudo bash -c '{{ .Path }}'"
  }

  post-processor "manifest" {
    output = "VAR_ARTIFACT_VAR_LOCATION.json"
   }
}
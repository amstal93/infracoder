#!/bin/bash

## Service: Admin Library
## Operator: Oracle
## Location: <URL>
## Software: https://gitlab.com/tboettjer
## Image: Oracle Linux 7.7
## Version: <0.2> 
## Tags: [MGT]

# --- helper functions ---
error() {
    echo "$(tput setaf 1)--- ${SCRIPTNAME}: $@$(tput sgr 0)" >&2;
    exit 1;
}

success() {
    echo "$(tput setaf 2)--- ${SCRIPTNAME}: $@$(tput sgr 0)";
}

note() {
    echo "$(tput setaf 3)$@$(tput sgr 0)";
}

write_log () {
    echo ["${USER}"] [`date`] - ${*} | sudo tee -a /var/log/infracoder.log
}

echo_log() {
    echo "--- ${SCRIPTNAME}: $@";
    write_log "--- ${SCRIPTNAME}: $@";
}

error_log() {
    echo "$(tput setaf 1)--- ${SCRIPTNAME}: $@$(tput sgr 0)" >&2;
    write_log "--- ${SCRIPTNAME}: $@";
    exit 1;
}

success_log() {
    echo "$(tput setaf 2)--- ${SCRIPTNAME}: $@$(tput sgr 0)"
    write_log "--- ${SCRIPTNAME}: $@";
}

validate_name() {
    # --- "validate_name $EMAIL" returns $USERNAME
    local CONTENT=$1;
    NAME=${CONTENT%@*};
    echo ${NAME::8} | tr -dc '[:alnum:]\n\r' | tr '[:upper:]' '[:lower:]';
}

check_daemon() {
    local DAEMON=$1
    if pgrep -x "$DAEMON" >/dev/null
    then
      success "$DAEMON is running"
    else
      error "$DAEMON not running"
      # uncomment to start $DAEMON if stopped
      # systemctl start $DAEMON
    fi
}

# --- ocloud functions ---
init() {
    echo "updating operating system, this can take a while ...";
    export LANGUAGE=$(echo $LANG);
    export LC_ALL=$(echo $LANG);
    ntpdate -u de.pool.ntp.org;
    yum clean all;
    yum -y update;
    echo "adding linux packages ...";
    yum -d1 -y install unzip policycoreutils-python nmap;
    echo "operating system updated";
}

launch_service() {
    SERVICE_CONFIG=$1;
    echo "downloading service configuration";
    wget -P /tmp/ https://gitlab.com/tboettjer/infracoder/raw/master/artifacts/bash/${SERVICE_CONFIG};
    sudo -S chmod +x /tmp/${SERVICE_CONFIG};
    [[ -z "${SERVICE_CONFIG}" ]] && error-log "Did not load configuration file";
    echo "installing ${SERVICE_CONFIG}";
    sudo -S /tmp/${SERVICE_CONFIG};
}

# --- Instance Configuration (Oracle Linux 7.7) ---

activate_project() {
    mv /tmp/project.json /etc/ocloud/project.json;
    chmod 666 /etc/ocloud/project.json;
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
}  

create_admin() {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    PASSWORD="changeme";
    PASSPHRASE="oracle";
    TENANCY=$(jq -r '.client.configuration[] | .tenancy' ${OCLOUD_CONFIG});
    REGION=$(jq -r '.client.configuration[] | .region' ${OCLOUD_CONFIG});
    USERNAME=$(jq -r '.client.admin[] | .name' ${OCLOUD_CONFIG});
    EMAIL=$(jq -r '.client.admin[] | .email' ${OCLOUD_CONFIG});
    USERID=$(jq -r '.client.admin[] | .user' ${OCLOUD_CONFIG});
    echo " ######  creating admin ${USERNAME}  ###### ";
    egrep "^${USERNAME}" /etc/passwd >/dev/null;
    if [ $? -eq 0 ]; then
        echo "${USERNAME} exists!";
        exit 1;
    else
        SECRET=$(perl -e 'print crypt($ARGV[0], "password")' $PASSWORD);
        useradd -m -p $SECRET $USERNAME;
        [ $? -eq 0 ] && echo_log "${USERNAME} has been added to system as new admin user!" || echo_log "Failed to add a user!";
    fi
    # --- enable secure access ---
    usermod -aG wheel $USERNAME;
    # --- install git client ---
    echo "installing git client";
    yum install $YUM_OPTS git;
    su - ${USERNAME} -c "git config --global user.name ${USERNAME}";
    su - ${USERNAME} -c "git config --global user.email ${EMAIL}";
    su - ${USERNAME} -c "git config --global core.editor nano";
    success "Git $(git --version) installed";
    # --- install python3 ---
    echo "installing python3";
    yum install ${YUM_OPTS} python3 python3-pip;
    su - ${USERNAME} -c " \
        pip3 install --user --upgrade pip \
        ";
    echo 'export PATH=~/.local/bin:"${PATH}"' >> /home/${USERNAME}/.bash_profile;
    su - ${USERNAME} -c "\
        pip3 install --user flake8 \
        flake8-colors \
        flake8-comprehensions \
        flake8-docstrings \
        flake8-import-order; \
        ";
    success "$(python3 --version) installed";
    # --- install OCI CLI in "PATH=$PATH:</home/$USERNAME/bin/oci>" is appended to /home/$USERNAME/.bashrc
    echo "installing OCI CLI";
    cd /home/${USERNAME}/;
    mkdir /home/${USERNAME}/.oci;
    wget -P /home/${USERNAME}/ https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh;
    chmod +x /home/${USERNAME}/install.sh;
    chown -R $USERNAME:$USERNAME /home/$USERNAME/;
    su - ${USERNAME} -c "/home/${USERNAME}/install.sh --accept-all-defaults";
    su - ${USERNAME} -c "/home/${USERNAME}/bin/oci setup oci-cli-rc";
    su - ${USERNAME} -c "/home/${USERNAME}/bin/oci setup keys --overwrite --passphrase ${PASSPHRASE}";
    PASS=$(openssl rsa -pubout -outform DER -in /home/${USERNAME}/.oci/oci_api_key.pem -passin pass:${PASSPHRASE} | openssl md5 -c | grep stdin | tr -d ' ');
    FINGERPRINT=${PASS##*=};
    wget -P /home/${USERNAME}/.oci https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/config;
    sed -i 's/VAR_TENANCY/'"${TENANCY}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_FINGERPRINT/'"${FINGERPRINT}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_REGION/'"${REGION}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_USER/'"${USERID}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_PHRASE/'"${PASSPHRASE}"'/' /home/${USERNAME}/.oci/config;
    chown -R $USERNAME:$USERNAME /home/${USERNAME}/;
    su - ${USERNAME} -c "/home/${USERNAME}/bin/oci setup repair-file-permissions --file /home/${USERNAME}/.oci/config";
    su - ${USERNAME} -c "export OCI_CLI_SUPPRESS_FILE_PERMISSIONS_WARNING=True";
    success "OCI-CLI installed, Version: $(/home/${USERNAME}/bin/oci -v)";
    # --- activating ssh key
    echo "activating ssh keys";
    mkdir /home/$USERNAME/.ssh && sudo chown $USERNAME:$USERNAME /home/$USERNAME/.ssh/;
    mv /tmp/id_rsa* /home/$USERNAME/.ssh/;
    [[ -f "/home/${USERNAME}/.ssh/id_rsa.pub" ]] && success "rsa key pair activated";
    [[ -f "/home/${USERNAME}/.ssh/authorized_keys" ]] && rm -r /home/$USERNAME/.ssh/authorized_keys;
    touch /home/$USERNAME/.ssh/authorized_keys && sudo chown $USERNAME:$USERNAME /home/$USERNAME/.ssh/authorized_keys;
    cat /home/$USERNAME/.ssh/id_rsa.pub >> /home/$USERNAME/.ssh/authorized_keys;
    [[ -f "/home/${USERNAME}/.ssh/authorized_keys" ]] && success "rsa key authorized";
    chmod 700 /home/$USERNAME/.ssh;
    chmod 644 /home/$USERNAME/.ssh/authorized_keys;
    chmod 600 /home/$USERNAME/.ssh/id_rsa;
    chmod 644 /home/$USERNAME/.ssh/id_rsa.pub;
    sudo cp /home/$USERNAME/.ssh/id_rsa* /root/.ssh
    echo "AllowUsers $USERNAME" >> /etc/ssh/sshd_config;
    restorecon -R -v /home/$USERNAME/.ssh;
    systemctl restart sshd.service;
    check_daemon sshd;
}

create_cert() {
    PUBURL=$1;
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    EMAIL=$(jq -r '.client.admin[] | .email' ${OCLOUD_CONFIG});
    PROJECT=$(jq -r '.client.configuration[] | .project' ${OCLOUD_CONFIG});
    #certbot --version;
    certbot-auto certonly --standalone --preferred-challenges --agree-tos http -d $PUBURL -n -m $EMAIL;
    wget -P /etc/letsencrypt/renewal-hooks/deploy/ https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/cert_renew.sh;
    echo "0 0,12 * * * root python -c 'import random; import time; time.sleep(random.random() * 3600)' && /usr/local/bin/certbot-auto renew" | sudo tee -a /etc/crontab > /dev/null
} 

create_user() {
    USERNAME=$1;
    USERID=$2;
    PASSWORD="changeme";
    PASSPHRASE="oracle";
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    TENANCY=$(jq -r '.client.configuration[] | .tenancy' ${OCLOUD_CONFIG});
    COMPARTMENT=$(jq -r '.client.configuration[] | .compartment' ${OCLOUD_CONFIG});
    REGION=$(jq -r '.client.configuration[] | .region' ${OCLOUD_CONFIG});
    echo " ------  creating user ${USERNAME}  ------ ";
    egrep "^${USERNAME}" /etc/passwd >/dev/null;
    if [ $? -eq 0 ]; then
        echo "${USERNAME} exists!";
        exit 1;
    else
        SECRET=$(perl -e 'print crypt($ARGV[0], "password")' $PASSWORD);
        useradd -m -p $SECRET $USERNAME;
        [ $? -eq 0 ] && echo "${USERNAME} has been added to system as new admin user!" || echo "Failed to add a user!";
    fi
    # --- enable secure access ---
    usermod -aG wheel $USERNAME;
    # --- install git client ---
    echo "configure git";
    su - ${USERNAME} -c "git config --global user.name ${USERNAME}";
    su - ${USERNAME} -c "git config --global user.email ${EMAIL}";
    su - ${USERNAME} -c "git config --global core.editor nano";
    # --- install python3 ---
    echo "installing python3";
    yum install ${YUM_OPTS} python3 python3-pip python3-wheel;
    su - ${USERNAME} -c " \
        pip3 install --user --upgrade pip \
        ";
    echo 'export PATH=~/.local/bin:"${PATH}"' >> /home/${USERNAME}/.bash_profile;
    su - ${USERNAME} -c "\
        pip3 install --user flake8 \
        flake8-colors \
        flake8-comprehensions \
        flake8-docstrings \
        flake8-import-order; \
        ";
    # --- install OCI CLI in "PATH=$PATH:</home/$USERNAME/bin/oci>" is appended to /home/$USERNAME/.bashrc
    echo "installing OCI CLI";
    cd /home/${USERNAME}/;
    mkdir /home/${USERNAME}/.oci;
    wget -P /home/${USERNAME}/ https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh;
    chmod +x /home/${USERNAME}/install.sh;
    chown -R $USERNAME:$USERNAME /home/$USERNAME/;
    su - ${USERNAME} -c "/home/${USERNAME}/install.sh --accept-all-defaults";
    su - ${USERNAME} -c "/home/${USERNAME}/bin/oci setup oci-cli-rc ";
    su - ${USERNAME} -c "/home/${USERNAME}/bin/oci setup keys --overwrite --passphrase ${PASSPHRASE}";
    PASS=$(openssl rsa -pubout -outform DER -in /home/${USERNAME}/.oci/oci_api_key.pem -passin pass:${PASSPHRASE} | openssl md5 -c | grep stdin | tr -d ' ');
    FINGERPRINT=${PASS##*=};
    wget -P /home/${USERNAME}/.oci https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/config;
    sed -i 's/VAR_TENANCY/'"${TENANCY}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_FINGERPRINT/'"${FINGERPRINT}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_REGION/'"${REGION}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_USER/'"${USERID}"'/' /home/${USERNAME}/.oci/config;
    sed -i 's/VAR_PHRASE/'"${PASSPHRASE}"'/' /home/${USERNAME}/.oci/config;
    chown -R $USERNAME:$USERNAME /home/${USERNAME}/;
    su - ${USERNAME} -c "/home/${USERNAME}/bin/oci setup repair-file-permissions --file /home/${USERNAME}/.oci/config";
    su - ${USERNAME} -c "export OCI_CLI_SUPPRESS_FILE_PERMISSIONS_WARNING=True";
    # --- creating ssh key ---
    echo "activating ssh keys";
    mkdir -p /home/$USERNAME/.ssh && sudo chown $USERNAME:$USERNAME /home/$USERNAME/.ssh/;
    ssh-keygen -o -t rsa -b 2048 -C "$EMAIL" -q -N "" -f /home/${USERNAME}/.ssh/id_rsa;
    chmod 600 /home/$USERNAME/.ssh/id_rsa;
    chmod 644 /home/$USERNAME/.ssh/id_rsa.pub;
    [[ -f "/home/${USERNAME}/.ssh/id_rsa.pub" ]] && echo "rsa key pair created";
    [[ -f "/home/${USERNAME}/.ssh/authorized_keys" ]] && rm -r /home/$USERNAME/.ssh/authorized_keys;
    touch /home/$USERNAME/.ssh/authorized_keys && sudo chown $USERNAME:$USERNAME /home/$USERNAME/.ssh/authorized_keys;
    cat /home/$USERNAME/.ssh/id_rsa.pub >> /home/$USERNAME/.ssh/authorized_keys;
    chmod 644 /home/$USERNAME/.ssh/authorized_keys;
    chmod 700 /home/$USERNAME/.ssh;
    [[ -f "/home/${USERNAME}/.ssh/authorized_keys" ]] && echo "rsa key authorized";
    echo "AllowUsers $USERNAME" >> /etc/ssh/sshd_config;
    restorecon -R -v /home/$USERNAME/.ssh;
    systemctl restart sshd.service;
}

configure_ssh() {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    USERNAME=$(jq -r '.client.admin[] | .name' ${OCLOUD_CONFIG});
    echo "activating ssh keys";
    mkdir /home/$USERNAME/.ssh && sudo chown $USERNAME:$USERNAME /home/$USERNAME/.ssh/;
    mv /tmp/id_rsa* /home/$USERNAME/.ssh/;
    [[ -f "/home/${USERNAME}/.ssh/id_rsa.pub" ]] && success "rsa key pair activated";
    
    rm -r authorized_keys;
    touch /home/$USERNAME/.ssh/authorized_keys && sudo chown $USERNAME:$USERNAME /home/$USERNAME/.ssh/authorized_keys;
    cat /home/$USERNAME/.ssh/id_rsa.pub >> /home/$USERNAME/.ssh/authorized_keys;
    [[ -f "/home/${USERNAME}/.ssh/authorized_keys" ]] && success "rsa key authorized";

    chmod 700 /home/$USERNAME/.ssh;
    chmod 644 /home/$USERNAME/.ssh/authorized_keys;
    chmod 600 /home/$USERNAME/.ssh/id_rsa;
    chmod 644 /home/$USERNAME/.ssh/id_rsa.pub;
    sudo cp /home/$USERNAME/.ssh/id_rsa* /root/.ssh
    echo "AllowUsers $USERNAME" >> /etc/ssh/sshd_config;

    restorecon -R -v /home/$USERNAME/.ssh;
    systemctl restart sshd.service;
    check_daemon sshd;
}

configure_firewall () {
    OPEN_PORTS=($(echo "${PORTS[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '));
    OPEN_NETSERVICES=($(echo "${NETSERVICES[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '));
    echo "configuring firewall";

    for PORT in "${OPEN_PORTS[@]}"; do
        firewall-cmd --zone=public --add-port=$PORT/tcp --permanent;
    done

    for NETSERVICE in "${OPEN_NETSERVICES[@]}"; do
        firewall-cmd --zone=public --add-service=$NETSERVICE --permanent;
    done

    firewall-cmd --reload;
    success "Ports ${OPEN_PORTS[*]} opened";
    success "Services added to the firewall: ${OPEN_NETSERVICES[*]}";
}

enable_epel() {
    echo "enabling EPEL";
    wget https://yum.oracle.com/RPM-GPG-KEY-oracle-ol7 -O /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle;
    gpg --quiet --with-fingerprint /etc/pki/rpm-gpg/RPM-GPG-KEY-oracle;
    wget -P /etc/yum.repos.d/ https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/ol7-temp.repo;
    yum -y install oracle-epel-release-el7 oracle-nodejs-release-el7;
    mv /etc/yum.repos.d/ol7-temp.repo /etc/yum.repos.d/ol7-temp.repo.disabled;
    echo "EPEL enablement completed";
}

update_ocloud() {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    GITURL=$(jq -r '.client.configuration[] | .git' ${OCLOUD_CONFIG});
    GITNAME=${GITURL##*:};
    REPO=$(echo ${GITNAME##*/} | cut -d "." -f 1);
    ntpdate -u de.pool.ntp.org;
    yum clean all;
    yum -y update;
    wget -q https://gitlab.com/tboettjer/infracoder/raw/master/.admin/ocloud.sh -O /usr/local/bin/ocloud;
    chmod +x /usr/local/bin/ocloud;
    wget -q https://gitlab.com/tboettjer/infracoder/raw/master/.admin/library/admin.lib.sh -O /usr/local/bin/admin.lib.sh;
    chown root:root /usr/local/bin/admin.lib.sh;
    wget -q https://gitlab.com/tboettjer/infracoder/raw/master/.admin/library/oci.lib.sh -O /usr/local/bin/oci.lib.sh;
    chown root:root /usr/local/bin/oci.lib.sh;
    wget -q https://gitlab.com/tboettjer/infracoder/raw/master/artifacts/bash/install.lib.sh -O /usr/local/bin/install.lib.sh;
    chown root:root /usr/local/bin/install.lib.sh;
    success "ocloud updated";
}

update_repo() {
    echo "updating git as ${SUDO_USER}";
    cd /home/${SUDO_USER}/${REPO}/;
    git pull upstream master;
    git push origin master;
}